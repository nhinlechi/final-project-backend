const AppError = require('../utils/appError');

exports.isObjectNull = (data) => {
    if (!data) throw new AppError(404, 'Data not found');
};
