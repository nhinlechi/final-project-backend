const swaggerJSDoc = require('swagger-jsdoc');

const swaggerDefinition = {
    openapi: '3.0.0',
    info: {
        title: 'Together',
        description: 'This is a Api for Together.',
        contact: {
            name: 'Đinh Ngọc',
            url: 'https://www.facebook.com/ngoc861999',
            email: 'togetherapplicationservice@gmail.com',
        },
        version: '1.0.0',
    },
    servers: [
        {
            url: 'https://togetherapis.herokuapp.com/api/v1',
            description: 'HOST SERVER',
        },
        {
            url: 'http://localhost:8080/api/v1',
            description: 'DEVELOPMENT SERVER',
        },
        {
            url: 'https://api.togetherapplication.online/api/v1',
            description: 'PRODUCTION SERVER',
        },
    ],
};

// options for the swagger docs
const options = {
    // import swaggerDefinitions
    swaggerDefinition,
    // path to the API docs
    apis: ['./src/docs/**/*.yaml'],
};
// initialize swagger-jsdoc
exports.swaggerSpec = swaggerJSDoc(options);
