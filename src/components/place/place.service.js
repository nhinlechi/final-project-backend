const Place = require('./place.model');
const AppError = require('../../utils/appError');

const degrees2radians = (degrees) => {
    const pi = Math.PI;
    return degrees * (pi / 180);
};

exports.distanceBetweenTwoPlace = (
    latitude1,
    longitude1,
    latitude2,
    longitude2
) => {
    // Radius of the Earth
    const R = 6373.0;

    // Coordinates
    const lat1 = degrees2radians(latitude1);
    const lon1 = degrees2radians(longitude1);
    const lat2 = degrees2radians(latitude2);
    const lon2 = degrees2radians(longitude2);

    // Change in coordinates
    const dlon = lon2 - lon1;
    const dlat = lat2 - lat1;

    // Haversine formula
    const a =
        Math.sin(dlat / 2) ** 2 +
        Math.cos(lat1) * Math.cos(lat2) * Math.sin(dlon / 2) ** 2;
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return R * c;
};

exports.getPlaceById = async (id) => await Place.findById(id);

const searchFull = async (type, page, rowsperpage, sort, textsearch) => {
    const total = await Place.find({
        $text: {
            $search: textsearch,
            $caseSensitive: false,
        },
        type,
    }).countDocuments();
    const places = await Place.find({
        $text: {
            $search: textsearch,
            $caseSensitive: false,
        },
        type,
    })
        .sort(sort)
        .limit(rowsperpage)
        .skip((page - 1) * rowsperpage);
    return { total, places };
};

const searchPartial = async (type, page, rowsperpage, sort, textsearch) => {
    const total = await Place.find({
        $or: [
            { name: new RegExp(textsearch, 'gi') },
            { address: new RegExp(textsearch, 'gi') },
        ],
        type,
    }).countDocuments();
    const places = await Place.find({
        $or: [
            { name: new RegExp(textsearch, 'gi') },
            { address: new RegExp(textsearch, 'gi') },
        ],
        type,
    })
        .sort(sort)
        .limit(rowsperpage)
        .skip((page - 1) * rowsperpage);
    return { total, places };
};

const seachPlace = async (type, page, rowsperpage, sort, textsearch) => {
    const data = await searchPartial(type, page, rowsperpage, sort, textsearch);
    if (data.total) return data;
    if (data.total === 0)
        return searchFull(type, page, rowsperpage, sort, textsearch);
    return 'Error';
};

exports.getPlaces = async (data) => {
    const { type = /^/, textsearch = /^/ } = data;
    const { page, rowsperpage, sort, sortby } = data;
    const sortquery = sort ? { [sortby]: parseInt(sort, 10) } : { photos: -1 };

    return await seachPlace(
        type,
        parseInt(page, 10),
        parseInt(rowsperpage, 10),
        sortquery,
        textsearch
    );
};

exports.getCommentPlaceById = async (id) => {
    const place = Place.findById(id)
        .select('+comment -location -address -icon -name -photos -type')
        .populate('comment.user', 'avatar lastname firstname');
    if (!place) throw new AppError(404, 'Not found place');
    return place;
};

exports.commentPlace = async (user, data) => {
    const { id, comment } = data;
    const rating = parseFloat(data.rating);

    const place = await Place.findById(id);
    if (!place) throw new AppError(404, 'Not found place');

    return await Place.findByIdAndUpdate(
        id,
        {
            $push: {
                comment: {
                    user,
                    comment,
                    rating,
                    created_comment: new Date().getTime(),
                },
            },
            rating:
                (place.rating * place.user_ratings_total + rating) /
                (place.user_ratings_total + 1),
            user_ratings_total: place.user_ratings_total + 1,
        },
        {
            new: true,
        }
    )
        .select('+comment -location -address -icon -name -photos -type')
        .populate('comment.user', 'avatar lastname firstname');
};

exports.getPlacesInViewPort = async (data) => {
    const latlefttop = parseFloat(data.lat_left_top);
    const longlefttop = parseFloat(data.long_left_top);
    const latrightbottom = parseFloat(data.lat_right_bottom);
    const longrightbottom = parseFloat(data.long_right_bottom);
    const places = await Place.find({
        $and: [
            {
                'location.lat': {
                    $gt: latrightbottom,
                    $lt: latlefttop,
                },
            },
            {
                'location.lng': {
                    $gt: longlefttop,
                    $lt: longrightbottom,
                },
            },
        ],
    }).limit(100);
    return { places, total: places.length };
};
