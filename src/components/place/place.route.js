const router = require('express').Router();
const placeController = require('./place.controller.js');

router.get('/getplacebyid/:id', placeController.getPlaceById);
router.get('/getcommentsplacebyid/:id', placeController.getCommentPlaceById);
router.get('/getplacesinviewport', placeController.getPlaceInViewport);
router.get('/', placeController.getPlaces);
router.patch('/comment', placeController.commentPlace);

module.exports = router;
