const { catchAsync } = require('../../utils/catchAsync');
const { QUERY, UPDATE } = require('../../middlewares/type-response');
const { successResponse } = require('../../middlewares/success-response');
const {
    getPlaces,
    getPlaceById,
    getCommentPlaceById,
    commentPlace,
    getPlacesInViewPort,
} = require('./place.service');
const {
    getPlacesValidation,
    commentValidation,
    getplacesInViewPortValidation,
} = require('../../validations/place.validation');
const { idValidation } = require('../../validations/common.validation');

exports.getPlaces = catchAsync(async (req, res) => {
    await getPlacesValidation(req.query);
    const places = await getPlaces(req.query);
    successResponse(res, QUERY, places);
});

exports.getPlaceById = catchAsync(async (req, res) => {
    await idValidation({ id: req.params.id });
    const place = await getPlaceById(req.params.id);
    successResponse(res, QUERY, place);
});

exports.getCommentPlaceById = catchAsync(async (req, res) => {
    await idValidation({ id: req.params.id });
    const place = await getCommentPlaceById(req.params.id);
    successResponse(res, QUERY, place);
});

exports.commentPlace = catchAsync(async (req, res) => {
    await commentValidation(req.body);
    const place = await commentPlace(req.user._id, req.body);
    successResponse(res, UPDATE, place);
});

exports.getPlaceInViewport = catchAsync(async (req, res) => {
    await getplacesInViewPortValidation(req.query);
    const place = await getPlacesInViewPort(req.query);
    successResponse(res, QUERY, place);
});
