const mongoose = require('mongoose');

const PlaceSchema = new mongoose.Schema(
    {
        location: {
            type: {
                lat: { type: Number },
                lng: { type: Number },
            },
            required: true,
        },
        address: {
            type: String,
            default: '',
        },
        icon: {
            type: String,
        },
        name: {
            type: String,
            required: true,
        },
        photos: {
            type: Array,
            default: [],
        },
        type: {
            type: String,
        },
        rating: {
            type: Number,
            default: 0,
        },
        user_ratings_total: {
            type: Number,
            default: 0,
        },
        comment: {
            type: [
                {
                    user: {
                        type: String,
                        ref: 'User',
                    },
                    comment: {
                        type: String,
                    },
                    rating: {
                        type: Number,
                    },
                    created_comment: {
                        type: Number,
                    },
                    _id: false,
                },
            ],
            default: [],
            select: false,
        },
        status: {
            type: Number,
            default: 1,
            select: false,
        },
    },
    {
        collection: 'place',
    }
);

PlaceSchema.index(
    { name: 'text', address: 'text' },
    { weights: { name: 4, address: 1 } }
);

module.exports = mongoose.model('Place', PlaceSchema);
