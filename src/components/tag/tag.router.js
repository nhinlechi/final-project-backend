const router = require('express').Router();
const tagController = require('./tag.controller');

router.get('/', tagController.getAllTag);

module.exports = router;
