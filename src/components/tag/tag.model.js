const mongoose = require('mongoose');

const TagSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
            minlength: 1,
            maxlength: 30,
        },
        type: {
            // 1: tag group, 2: tag other
            type: Number,
            default: 1,
        },
    },
    {
        collection: 'tag',
    }
);

module.exports = mongoose.model('Tag', TagSchema);
