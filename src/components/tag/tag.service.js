const Tag = require('./tag.model');

// Get all tag
exports.getAllTag = async () => Tag.find({ type: 3 }).select('-type -__v');
