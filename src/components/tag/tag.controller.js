const { catchAsync } = require('../../utils/catchAsync');
const { QUERY } = require('../../middlewares/type-response');
const { successResponse } = require('../../middlewares/success-response');
const { getAllTag } = require('./tag.service');

exports.getAllTag = catchAsync(async (req, res) => {
    const tags = await getAllTag();
    successResponse(res, QUERY, tags);
});
