const { ObjectId } = require('mongoose').Types;
const { event } = require('./event');
const { storeChat, getEventById } = require('../event/event.service');
const { isGroupMemmberId } = require('../group/group.service');

const defaultRs = (user, roomId) => ({
    roomId,
    id: user._id,
    firstname: user.firstname,
    lastname: user.lastname,
    avatar: user.avatar,
    created: new Date().getTime(),
});

const storeUser = (socketId, store, userId, roomId) => {
    store[roomId] = store[roomId] || [];
    store[roomId].push({ socketId, userId });
};

const setupAndStore = (socket, adapter, roomId, userId) => {
    adapter.member = adapter.member || {};
    storeUser(socket.id, adapter.member, userId, roomId);
};

const joinRoom = (socket, roomId, user, adapter) => {
    setupAndStore(socket, adapter, roomId, user._id);
    socket.join(roomId);
    socket.broadcast
        .to(roomId)
        .emit(event.SERVER_JOIN_CHAT, defaultRs(user, roomId));
};

const storeMessage2Database = (roomId, user, data) => {
    // STORE BD
    storeChat({
        sender: user._id,
        eventid: roomId,
        message: data.message,
        type: data.type,
    });
};

const sendChat = (socket, roomId, user, data) => {
    const ev = event.SERVER_SENT_CHAT;
    storeMessage2Database(roomId, user, data);
    socket.to(roomId).emit(ev, { data, ...defaultRs(user, roomId) });
};

const startTyping = (socket, roomId, user) => {
    const ev = event.SERVER_START_TYPING;
    socket.broadcast.to(roomId).emit(ev, defaultRs(user, roomId));
};

const endTyping = (socket, roomId, user) => {
    const ev = event.SERVER_END_TYPING;
    socket.broadcast.to(roomId).emit(ev, defaultRs(user, roomId));
};

const removeUser = (store, socketId, roomId) => {
    if (store[roomId]) {
        for (let index = 0; index < store[roomId].length; index += 1) {
            if (store[roomId][index].socketId === socketId) {
                store[roomId].splice(index, 1);
                index -= 1;
            }
        }
    }
};

const leaveRoom = (socket, roomId, user, store) => {
    socket.broadcast
        .to(roomId)
        .emit(event.SERVER_LEAVE_CHAT, defaultRs(user, roomId));
    socket.leave(roomId);
    if (store) removeUser(store, socket.id, roomId);
};

const isObjectId = (id) => {
    const valid = ObjectId.isValid(id);
    return valid && new ObjectId(id).toHexString() === id;
};

const validationJoinChat = async (userId, eventId, callback) => {
    if (!isObjectId(eventId)) {
        if (callback) callback('roomId invalid');
        return false;
    }
    const eventExits = await getEventById(eventId);
    if (!eventExits) {
        if (callback) callback('Event not found!');
        return false;
    }

    try {
        await isGroupMemmberId(eventExits.groupid, userId);
    } catch (error) {
        if (callback) callback(error.message);
        return false;
    }
    return true;
};

const checkSocketJoinRoom = (socket, roomId, adapter, callback) => {
    const socketIds = adapter.rooms.get(roomId) || new Set();
    if (socketIds.has(socket.id)) return true;
    if (callback) callback('Please join chat.');
    return false;
};

exports.listen = (socket, user, adapter) => {
    let sids = adapter.sids.get(socket.id) || new Set();

    socket.on(event.CLIENT_JOIN_CHAT, async (data, callback) => {
        if (!data) return;
        const { roomId } = data;
        if (!(await validationJoinChat(user._id, roomId, callback))) return;
        joinRoom(socket, roomId, user, adapter);
        sids = adapter.sids.get(socket.id);
    });

    socket.on(event.CLIENT_START_TYPING, (data, callback) => {
        if (!data) return;
        const { roomId } = data;
        if (!checkSocketJoinRoom(socket, roomId, adapter, callback)) return;
        startTyping(socket, roomId, user);
    });

    socket.on(event.CLIENT_END_TYPING, (data, callback) => {
        if (!data) return;
        const { roomId } = data;
        if (!checkSocketJoinRoom(socket, roomId, adapter, callback)) return;
        endTyping(socket, roomId, user);
    });

    socket.on(event.CLIENT_SENT_CHAT, (data, callback) => {
        if (!data) return;
        const { message, type, roomId } = data;
        if (!checkSocketJoinRoom(socket, roomId, adapter, callback)) return;
        if (message && type) sendChat(socket, roomId, user, { message, type });
        else if (callback) callback('Message or type dont valid!');
    });

    socket.on(event.CLIENT_LEAVE_CHAT, (data, callback) => {
        if (!data) return;
        const { roomId } = data;
        const store = adapter.member;
        if (!checkSocketJoinRoom(socket, roomId, adapter, callback)) return;
        leaveRoom(socket, roomId, user, store);
        sids = adapter.sids.get(socket.id);
    });

    socket.on(event.DISCONNECT, () => {
        const store = adapter.member;
        for (const sid of sids) {
            if (sid !== socket.id) leaveRoom(socket, sid, user, store);
        }
    });
};
