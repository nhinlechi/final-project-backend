const redisAdapter = require('socket.io-redis');
const { Server } = require('socket.io');
const { authenSocket } = require('../../middlewares/authentication');
const { event } = require('./event');
const ChatSocket = require('./chat-socket');
const ChatEventSocket = require('./chat-event-socket');
const VideoCall = require('./videocall-socket');
const logger = require('../../utils/logger');

module.exports = (app) => {
    const io = new Server(app, {
        transports: ['websocket', 'polling'],
        cors: true,
        origins: '*',
        rememberUpgrade: true,
    });

    try {
        const host = process.env.REDIS_HOST;
        const port = Number(process.env.REDIS_PORT);
        const password = process.env.REDIS_PASSWORD;
        io.adapter(redisAdapter({ host, port, password }));
    } catch (err) {
        logger.error(err);
    }

    const chatNsps = io.of('/chat');
    chatNsps.use(authenSocket).on(event.CONNECT, async (socket) => {
        const { user } = socket.handshake.query;
        ChatSocket.listen(socket, user, chatNsps.adapter);
    });

    const videoCallNsps = io.of('/videocall');
    videoCallNsps.use(authenSocket).on(event.CONNECT, async (socket) => {
        const { user } = socket.handshake.query;
        VideoCall.listen(socket, user, videoCallNsps.adapter, videoCallNsps);
    });

    const chatEventNsps = io.of('/chatevent');
    chatEventNsps.use(authenSocket).on(event.CONNECT, async (socket) => {
        const { user } = socket.handshake.query;
        ChatEventSocket.listen(socket, user, chatEventNsps.adapter);
    });

    return io;
};
