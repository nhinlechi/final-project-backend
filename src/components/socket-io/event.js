exports.event = {
    // Common
    CONNECT: 'connect',
    DISCONNECT: 'disconnect',
    // Chat
    CLIENT_JOIN_CHAT: 'client_join_chat',
    SERVER_JOIN_CHAT: 'server_join_chat',
    CLIENT_LEAVE_CHAT: 'client_leave_chat',
    SERVER_LEAVE_CHAT: 'server_leave_chat',
    CLIENT_SENT_CHAT: 'client_sent_chat',
    SERVER_SENT_CHAT: 'server_sent_chat',
    CLIENT_START_TYPING: 'client_start_typing',
    SERVER_START_TYPING: 'server_start_typing',
    CLIENT_END_TYPING: 'client_end_typing',
    SERVER_END_TYPING: 'server_end_typing',

    // video call
    CLIENT_MAKE_CALL: 'client_make_call',
    CLIENT_JOIN_CALL_FROM_NOTI: 'client_join_call_from_noti',
    CLIENT_SEND_OFFER: 'client_send_offer',
    CLIENT_SEND_ANSWER: 'client_answer',
    CLIENT_SEND_CANDIDATE: 'client_candidate',
    CLIENT_LEAVE_VIDEO_CALL: 'client_leave_video_call',

    SEVER_SEND_RECEIVER_ONLINE: 'server_send_receiver_online',
    SERVER_SEND_OFFER: 'server_send_offer',
    SERVER_SEND_ANSWER: 'server_send_answer',
    SERVER_SEND_CANDIDATE: 'server_send_candidate',
    SERVER_LEAVE_VIDEO_CALL: 'server_leave_video_call',
};
