const { event } = require('./event');
const { sendNotification } = require('../notification/notication.service');

const storeUser = (socket, adapter, userId) => {
    adapter.member[userId] = socket.id;
};

const getSocketIdUser = (userId, adapter) => adapter.member[userId];

const sendReceiverOnline = (videoCallNsps, receiverSocketId) => {
    videoCallNsps.to(receiverSocketId).emit(event.SEVER_SEND_RECEIVER_ONLINE);
};

const sendOffer = (videoCallNsps, receiverSocketId, caller, offer) => {
    videoCallNsps
        .to(receiverSocketId)
        .emit(event.SERVER_SEND_OFFER, { caller, offer });
};
const sendAnswer = (videoCallNsps, receiverSocketId, isaccepted, answer) => {
    videoCallNsps
        .to(receiverSocketId)
        .emit(event.SERVER_SEND_ANSWER, { isaccepted, answer });
};

const sendCandidate = (videoCallNsps, receiverSocketId, candidate) => {
    videoCallNsps
        .to(receiverSocketId)
        .emit(event.SERVER_SEND_CANDIDATE, { candidate });
};

const removeUser = (adapter, userId) => {
    if (adapter.member) delete adapter.member[userId];
};

const leave = (videoCallNsps, receiverSocketId) => {
    videoCallNsps.to(receiverSocketId).emit(event.SERVER_LEAVE_VIDEO_CALL);
};

exports.listen = (socket, user, adapter, videoCallNsps) => {
    adapter.member = adapter.member || {};
    storeUser(socket, adapter, user._id);
    socket.on(event.CLIENT_MAKE_CALL, async (data, callback) => {
        if (!data) return;
        const { receiverId } = data;
        socket.otherUserId = receiverId;

        const receiverSocketId = getSocketIdUser(receiverId, adapter);
        if (!receiverSocketId) {
            try {
                await sendNotification(
                    [receiverId],
                    '9',
                    { caller: user },
                    null,
                    false
                );
                if (callback) callback('Connecting...');
            } catch (error) {
                if (callback) callback('reciever not save firebase toekn');
            }
        } else sendReceiverOnline(videoCallNsps, socket.id);
    });

    socket.on(event.CLIENT_JOIN_CALL_FROM_NOTI, (data, callback) => {
        if (!data) return;
        const { callerId } = data;
        const callerSocketId = getSocketIdUser(callerId, adapter);
        if (callerSocketId) {
            socket.otherUserId = callerId;
            sendReceiverOnline(videoCallNsps, callerSocketId);
        } else if (callback) callback('caller offline or callerId invalid');
    });

    socket.on(event.CLIENT_SEND_OFFER, (data, callback) => {
        if (!data) return;
        const { offer } = data;
        const receiverSocketId = getSocketIdUser(socket.otherUserId, adapter);
        if (receiverSocketId) {
            sendOffer(videoCallNsps, receiverSocketId, user, offer);
        } else if (callback) callback('reciever offline');
    });

    socket.on(event.CLIENT_SEND_ANSWER, (data) => {
        if (!data) return;
        const { callerId, isaccepted, answer } = data;
        const receiverSocketId = getSocketIdUser(callerId, adapter);
        if (receiverSocketId) {
            socket.otherUserId = callerId;
            sendAnswer(videoCallNsps, receiverSocketId, isaccepted, answer);
        }
    });

    socket.on(event.CLIENT_SEND_CANDIDATE, (data) => {
        if (!data) return;
        const { candidate } = data;
        const receiverSocketId = getSocketIdUser(socket.otherUserId, adapter);
        if (receiverSocketId)
            sendCandidate(videoCallNsps, receiverSocketId, candidate);
    });

    socket.on(event.CLIENT_LEAVE_VIDEO_CALL, () => {
        removeUser(adapter, user._id);
        const receiverSocketId = getSocketIdUser(socket.otherUserId, adapter);
        if (receiverSocketId) leave(videoCallNsps, receiverSocketId);
        socket.otherUserId = null;
    });

    socket.on(event.DISCONNECT, () => {
        removeUser(adapter, user._id);
        const receiverSocketId = getSocketIdUser(socket.otherUserId, adapter);
        if (receiverSocketId) leave(videoCallNsps, receiverSocketId);
    });
};
