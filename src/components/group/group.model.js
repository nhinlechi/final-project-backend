const mongoose = require('mongoose');

const GroupSchema = new mongoose.Schema(
    {
        host: {
            type: String,
            required: true,
            ref: 'User',
        },
        name: {
            type: String,
            required: true,
            unique: true,
        },
        avatar: {
            type: String,
            default:
                'https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png',
        },
        description: {
            type: String,
            default: '',
        },
        tag: {
            type: [
                {
                    type: String,
                    ref: 'Tag',
                },
            ],
            default: [],
        },
        participant: {
            type: [
                {
                    type: String,
                    ref: 'User',
                },
            ],
            default: [],
        },
        size: {
            type: Number,
        },
        isprivate: {
            type: Boolean,
            default: false,
        },
        status: {
            // 0: nomal ,1: disalbe,2 :band
            type: Number,
            default: 0,
            select: false,
        },
        star: {
            type: Number,
            default: 0,
        },
        createdAt: {
            type: Number,
            default: Date.now,
        },
    },
    {
        collection: 'group',
    }
);

GroupSchema.index(
    { name: 'text', description: 'text' },
    { weights: { name: 4, description: 1 } }
);

module.exports = mongoose.model('Group', GroupSchema);
