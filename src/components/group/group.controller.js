const { catchAsync } = require('../../utils/catchAsync');
const {
    QUERY,
    CREATE,
    UPDATE,
    DELETE,
} = require('../../middlewares/type-response');
const { successResponse } = require('../../middlewares/success-response');
const {
    getAllGroup,
    getGroupById,
    getGroupOfUser,
    createGroup,
    requestJoinGroup,
    confirmJoinGroup,
    updateGroup,
    removeParticipant,
    leaveGroup,
} = require('./group.service');
const {
    createGroupValidation,
    getAllGroupValidation,
    requestJoinGroupValidation,
    confirmJoinGroupValidation,
    updateGroupValidation,
    removeParticipantValidation,
    leaveValidation,
} = require('../../validations/group.validation');
const { idValidation } = require('../../validations/common.validation');

exports.getAllGroup = catchAsync(async (req, res) => {
    await getAllGroupValidation(req.query);
    const listGroup = await getAllGroup(req.query);
    successResponse(res, QUERY, listGroup);
});

exports.getGroupOfUser = catchAsync(async (req, res) => {
    const group = await getGroupOfUser(req.user._id);
    successResponse(res, QUERY, group);
});

exports.getGroupById = catchAsync(async (req, res) => {
    await idValidation({ id: req.params.id });
    const group = await getGroupById(req.params.id);
    successResponse(res, QUERY, group);
});

exports.createGroup = catchAsync(async (req, res) => {
    await createGroupValidation(req.body);
    const group = await createGroup(req.user._id, req.body);
    successResponse(res, CREATE, group);
});

exports.updateGroup = catchAsync(async (req, res) => {
    await updateGroupValidation(req.body);
    const group = await updateGroup(req.user._id, req.body);
    successResponse(res, CREATE, group);
});

exports.requestJoinGroup = catchAsync(async (req, res) => {
    await requestJoinGroupValidation(req.body);
    const { group, status } = await requestJoinGroup(req.user._id, req.body);
    successResponse(res, UPDATE, group, status);
});

exports.confirmJoinGroup = catchAsync(async (req, res) => {
    await confirmJoinGroupValidation(req.body);
    const group = await confirmJoinGroup(req.user._id, req.body);
    successResponse(res, UPDATE, group);
});

exports.removeParticipant = catchAsync(async (req, res) => {
    await removeParticipantValidation(req.body);
    const group = await removeParticipant(req.user._id, req.body);
    successResponse(res, DELETE, group);
});

exports.leave = catchAsync(async (req, res) => {
    await leaveValidation(req.body);
    const group = await leaveGroup(req.user._id, req.body);
    successResponse(res, DELETE, group);
});
