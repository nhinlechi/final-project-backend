const router = require('express').Router();
const groupController = require('./group.controller.js');

router.get('/', groupController.getAllGroup);
router.get('/getgroupofuser', groupController.getGroupOfUser);
router.get('/getgroupbyid/:id', groupController.getGroupById);

router.patch('/', groupController.updateGroup);

router.post('/', groupController.createGroup);
router.post('/requestjoin', groupController.requestJoinGroup);
router.post('/confirmjoin', groupController.confirmJoinGroup);

router.delete('/removeParticipant', groupController.removeParticipant);
router.delete('/leave', groupController.leave);

module.exports = router;
