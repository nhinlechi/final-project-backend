const Group = require('./group.model');
const AppError = require('../../utils/appError.js');
const { isObjectNull } = require('../../common/error.detection');
const {
    sendNotification,
    checkNotificationExits,
    deleteNotification,
    getNotificationById,
} = require('../notification/notication.service');

// get a group and donnot care they in or not
const getGroupByIdInternal = (id) => Group.findById(id);

const isGroupHost = (userid, group) => {
    isObjectNull(group);
    if (group.host !== userid)
        throw new AppError(400, 'This user is not group host');
};

const isGroupParticipant = (userid, group) => {
    isObjectNull(group);
    if (!group.participant.includes(userid))
        throw new AppError(400, ' This user is not group participant');
};

const isNotInGroup = (userid, group) => {
    isObjectNull(group);
    if (group.host === userid || group.participant.includes(userid))
        throw new AppError(409, 'This user aready in group ');
};

// const isInGroup = (userid, group) => {
//     isObjectNull(group);
//     if (group.host !== userid && !group.participant.includes(userid))
//         throw new AppError(400, 'This user is not in group');
// };

// update with new group and update tag list
const updateGroupInternal = async (group) => {
    group = await Group.findByIdAndUpdate(group._id, group, {
        new: true,
    })
        .populate('participant', 'avatar firstname')
        .populate('tag')
        .populate('host', 'avatar firstname');
    return group;
};

const addParticipantToGroup = async (groupid, userid) => {
    const group = await Group.findById(groupid);
    isNotInGroup(userid, group);
    group.participant.push(userid);
    sendNotification([userid], '5', { groupid }, null, false);
    await updateGroupInternal(group);
    return group;
};

// validate in event
exports.isGroupHostId = async (groupid, userid) => {
    const group = await Group.findById(groupid);
    isObjectNull(group);
    if (group.host !== userid)
        throw new AppError(400, 'This user is not group host');
};

exports.isGroupMemmberId = async (groupid, userid) => {
    const group = await Group.findById(groupid);
    isObjectNull(group);
    if (!group.participant.includes(userid) && group.host !== userid)
        throw new AppError(400, ' This user is not group participants');
};

exports.createGroup = async (host, data) => {
    const { name, isprivate, description, avatar, tag } = data;
    let group = Group({ host, name, isprivate, description, avatar, tag });
    group = await group.save();
    group = Group.findById(group._id)
        .populate('participant', 'avatar firstname')
        .populate('tag')
        .populate('host', 'avatar firstname');
    return group;
};

exports.updateGroup = async (userid, data) => {
    const group = await getGroupByIdInternal(data.groupid);
    isGroupHost(userid, group);

    for (const [field, value] of Object.entries(data)) group[field] = value;
    return updateGroupInternal(group);
};

exports.getGroupById = async (id) => {
    const group = await Group.findById(id)
        .populate('participant', 'avatar firstname')
        .populate('tag')
        .populate('host', 'avatar firstname');
    isObjectNull(group);
    return group;
};

exports.getGroupByIdnotPopulate = async (id) => {
    const group = await Group.findById(id);
    return group;
};
exports.getAllGroup = async (data) => {
    const { page, rowsperpage, sort, textsearch, ...query } = data;
    if (textsearch)
        query.$text = {
            $search: textsearch,
            $caseSensitive: false,
        };
    const total = await Group.countDocuments(query);
    const groups = await Group.find(query)
        .sort({ participant: sort })
        .limit(rowsperpage)
        .skip((page - 1) * rowsperpage)
        .populate('participant', 'avatar firstname')
        .populate('tag')
        .populate('host', 'avatar firstname');
    return { total, groups };
};

exports.getGroupOfUser = async (userid) => {
    const groups = await Group.find({
        $or: [
            { host: userid },
            {
                participant: { $in: [userid] },
            },
        ],
    })
        .populate('participant', 'avatar firstname')
        .populate('tag')
        .populate('host', 'avatar firstname');
    const total = groups.length;
    return { total, groups };
};

exports.requestJoinGroup = async (userid, data) => {
    const { groupid, description } = data;
    const group = await getGroupByIdInternal(groupid);
    isNotInGroup(userid, group);

    if (group.isprivate) {
        await checkNotificationExits([group.host], userid, '4', groupid);
        await sendNotification(
            [group.host],
            '4',
            {
                serviceid: groupid,
                userid,
                description,
            },
            userid
        );
        return { data: null, status: '202' };
    }

    return await addParticipantToGroup(groupid, userid);
};

exports.confirmJoinGroup = async (userid, data) => {
    const { status, notificationid } = data;

    const noti = await getNotificationById(notificationid);
    isObjectNull(noti);
    await deleteNotification(notificationid);

    if (!status) return true;

    const group = await getGroupByIdInternal(noti.serviceid);
    await isGroupHost(userid, group);

    return await addParticipantToGroup(noti.serviceid, noti.data.userid);
};

// host
exports.removeParticipant = async (userid, data) => {
    const { groupid, participantid } = data;

    const group = await getGroupByIdInternal(groupid);
    isGroupHost(userid, group);
    isGroupParticipant(participantid, group);

    const participantIndex = group.participant.indexOf(participantid);
    group.participant.splice(participantIndex, 1);

    return await updateGroupInternal(group);
};

// participant
exports.leaveGroup = async (userid, data) => {
    const { groupid } = data;
    const group = await getGroupByIdInternal(groupid);
    isGroupParticipant(userid, group);

    const participantIndex = group.participant.indexOf(userid);
    group.participant.splice(participantIndex, 1);
    await updateGroupInternal(group);
    return true;
};
