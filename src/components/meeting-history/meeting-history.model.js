const mongoose = require('mongoose');

const meetingHistorySchema = new mongoose.Schema(
    {
        participants: {
            type: [
                {
                    type: String,
                    ref: 'User',
                },
            ],
            required: true,
        },
        matchid: {
            type: String,
            required: true,
            ref: 'Match',
        },
        placeid: {
            type: String,
            required: true,
            ref: 'Place',
        },
        datetime: {
            type: Number,
            default: new Date().getTime(),
        },
        status: {
            // 0: watiting to meet, 1: met, -1: cancel
            type: Number,
            default: 0,
        },
    },
    {
        collection: 'meeting-history',
    }
);

module.exports = mongoose.model('meeting-history', meetingHistorySchema);
