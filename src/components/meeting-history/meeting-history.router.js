const router = require('express').Router();
const meetingHistoryController = require('./meeting-history.controller');

router.get('/', meetingHistoryController.getMatchMeetingHistory);

module.exports = router;
