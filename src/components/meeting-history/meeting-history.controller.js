const { catchAsync } = require('../../utils/catchAsync');
const { QUERY } = require('../../middlewares/type-response');
const { successResponse } = require('../../middlewares/success-response');

const { getMeetingHistory } = require('./meeting-history.service');
const {
    getMeetingHisotyValidation,
} = require('../../validations/meeting-history.validate');

exports.getMatchMeetingHistory = catchAsync(async (req, res) => {
    await getMeetingHisotyValidation(req.query);
    const meettingHistory = await getMeetingHistory(req.user._id, req.query);
    successResponse(res, QUERY, meettingHistory);
});
