const cron = require('node-cron');
const { sendNotification } = require('../notification/notication.service');
const MeetingHistory = require('./meeting-history.model');

const updateMeetingInternal = async (meeting) => {
    meeting = await MeetingHistory.findByIdAndUpdate(meeting._id, meeting, {
        new: true,
    });
    return meeting;
};
exports.getMeetingHistoryById = async (id) => await MeetingHistory.findById(id);

exports.saveMeetingHistory = async (
    participants,
    matchid,
    datetime,
    placeid,
    status = 0
) => {
    const meetingHistory = new MeetingHistory({
        participants,
        matchid,
        datetime,
        placeid,
        status,
    });
    return await meetingHistory.save();
};

exports.getMeetingHistory = async (userid, data) => {
    const query = { participants: userid, ...data };
    return await MeetingHistory.find(query)
        .populate('placeid')
        .populate('participants', 'avatar firstname lastname');
};

cron.schedule('*/30 * * * *', async () => {
    // notify up comming meetings
    const current = Date.now();
    const meetings = await MeetingHistory.find({
        date: {
            $gte: current,
            $lt: current + 1000 * 3599, // 1 hour
        },
    });

    for (let i = 0; i < meetings.length; i += 1) {
        const durationInMinute = (meetings[i].date - current) / (1000 * 60);
        sendNotification(
            meetings[i].participants,
            '12',
            { meetingid: meetings[i]._id, durationInMinute },
            null,
            false
        );
    }

    // find and update status of MeetingHistory
    const goneMeetings = await MeetingHistory.find({
        date: {
            $lt: current,
        },
        status: 0,
    });

    for (let i = 0; i < goneMeetings.length; i += 1) {
        goneMeetings[i].status = 1;
        updateMeetingInternal(goneMeetings[i]);
    }
});
