// https://github.com/nodemailer/nodemailer/issues/611

const nodemailer = require('nodemailer');
const ForgotPassword = require('./forgotpassword.model');
const logger = require('../../utils/logger');
const { resetPassword } = require('../user/user.service');
const AppError = require('../../utils/appError.js');

const generateCode = () => {
    const min = 100000;
    const max = 999999;
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

const createForgotPassword = async (email, code) => {
    const timeout = new Date().getTime() + 600000;
    await ForgotPassword.findOneAndUpdate(
        { email },
        { code, timeout },
        { upsert: true }
    );
};

const sendCodeToEmail = (email, code) => {
    const user = process.env.EMAIL_ADMIN;
    const pass = process.env.PW_EMAIL;
    const transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: { user, pass },
    });

    const mailOptions = {
        from: user,
        to: email,
        subject: 'Forgot password from Together app',
        text: code.toString(),
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            logger.error(error);
        } else {
            logger.info(`Email sent: ${info.response}`);
        }
    });
};

exports.forgotPassword = async (data) => {
    const { email } = data;
    const code = generateCode();
    await createForgotPassword(email, code);
    await sendCodeToEmail(email, code);
    return 'Check code in email, 10 minute expried';
};

exports.resetNewPassword = async (data) => {
    const { code, email, newpassword } = data;
    const forgotPassword = await ForgotPassword.findOne({
        code,
        email,
        timeout: {
            $gte: new Date().getTime(),
        },
    });
    if (forgotPassword) {
        return await resetPassword(email, newpassword);
    }
    throw new AppError(400, 'Code or email incorrect');
};
