const mongoose = require('mongoose');

const ForgotPassword = new mongoose.Schema(
    {
        email: {
            type: String,
            unique: true,
        },
        code: {
            type: String,
            required: true,
        },
        timeout: {
            type: Number,
            default: new Date().getTime() + 600000,
        },
    },
    {
        collection: 'forgotpassword',
    }
);

module.exports = mongoose.model('ForgotPassword', ForgotPassword);
