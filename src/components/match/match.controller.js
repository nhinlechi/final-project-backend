const { catchAsync } = require('../../utils/catchAsync');
const {
    QUERY,
    CREATE,
    UPDATE,
    DELETE,
} = require('../../middlewares/type-response');
const { successResponse } = require('../../middlewares/success-response');
const {
    getMatchById,
    getMatchOfUser,
    getChatOfMatch,
    matchNow,
    matchLater,
    confirmMatch,
    sendPlaceRequest,
    deletePlaceRequest,
    confirmPlace,
    cancelRequestMatch,
    recommendPlaceForMatch,
    isInConfirmMatchProcess,
} = require('./match.service');
const {
    requestMatchValidation,
    getChatValidation,
    confirmMatchNowValidation,
    sendPlaceRequestValidation,
    confirmPlaceValidation,
} = require('../../validations/match.validation');
const { idValidation } = require('../../validations/common.validation');

exports.recommendPlaceForMatch = catchAsync(async (req, res) => {
    await idValidation({ id: req.params.id });
    const match = await recommendPlaceForMatch(req.params.id);
    successResponse(res, QUERY, match);
});

exports.isInConfirmMatchProcess = catchAsync(async (req, res) => {
    const match = await isInConfirmMatchProcess(req.user._id);
    successResponse(res, QUERY, match);
});

exports.cancelRequestMatch = catchAsync(async (req, res) => {
    const match = await cancelRequestMatch(req.user._id);
    successResponse(res, UPDATE, match);
});

exports.matchLater = catchAsync(async (req, res) => {
    await requestMatchValidation(req.body);
    const match = await matchLater(req.user._id, req.body);
    successResponse(res, CREATE, match);
});

exports.matchNow = catchAsync(async (req, res) => {
    await requestMatchValidation(req.body);
    const match = await matchNow(req.user._id, req.body);
    successResponse(res, CREATE, match);
});

exports.confirmMatch = catchAsync(async (req, res) => {
    await confirmMatchNowValidation(req.body);
    const match = await confirmMatch(req.user._id, req.body);
    successResponse(res, UPDATE, match);
});

exports.getMatch = catchAsync(async (req, res) => {
    await idValidation({ id: req.params.id });
    const match = await getMatchById(req.params.id);
    successResponse(res, QUERY, match);
});

exports.getMyMatch = catchAsync(async (req, res) => {
    const match = await getMatchOfUser(req.user._id);
    successResponse(res, QUERY, match);
});

exports.getChat = catchAsync(async (req, res) => {
    await getChatValidation(req.query);
    const chat = await getChatOfMatch(req.user._id, req.query);
    successResponse(res, QUERY, chat);
});

exports.sendPlaceRequest = catchAsync(async (req, res) => {
    await sendPlaceRequestValidation(req.body);
    const chat = await sendPlaceRequest(req.user._id, req.body);
    successResponse(res, UPDATE, chat);
});

exports.deletePlaceRequest = catchAsync(async (req, res) => {
    await idValidation({ id: req.body.notificationid });
    const chat = await deletePlaceRequest(req.user._id, req.body);
    successResponse(res, DELETE, chat);
});

exports.confirmPlace = catchAsync(async (req, res) => {
    await confirmPlaceValidation(req.body);
    const chat = await confirmPlace(req.user._id, req.body);
    successResponse(res, UPDATE, chat);
});
