const router = require('express').Router();
const matchController = require('./match.controller.js');

router.post('/matchnow', matchController.matchNow);
router.get('/checkconfirmprocess', matchController.isInConfirmMatchProcess);
router.get('/recommendplace/:id', matchController.recommendPlaceForMatch);
router.post('/cancelrequest', matchController.cancelRequestMatch);
router.post('/matchlater', matchController.matchLater);
router.patch('/confirmmatch', matchController.confirmMatch);
router.get('/', matchController.getMyMatch);
router.get('/chat', matchController.getChat);
router.get('/:id', matchController.getMatch);
router.post('/sendPlace', matchController.sendPlaceRequest);
router.post('/confirmPlace', matchController.confirmPlace);
router.delete('/deletePlace', matchController.deletePlaceRequest);

module.exports = router;
