const cron = require('node-cron');
const Match = require('./match.model');
const AppError = require('../../utils/appError.js');
const { getUserById } = require('../user/user.service');
const {
    sendNotification,
    updateNotification,
    getNotificationById,
    deleteNotification,
    isInConfirmMatchProcess,
} = require('../notification/notication.service');
const {
    createRequestMatch,
    findUserMatchNow,
    findListUserMatchedLater,
    cancelRequestMatch,
} = require('../request-match/request-match.service');
const { getPlaceById, getPlacesInViewPort } = require('../place/place.service');
const { isObjectNull } = require('../../common/error.detection');
const {
    saveMeetingHistory,
} = require('../meeting-history/meeting-history.service');

exports.getMatchById = (matchId) => Match.findById(matchId);

exports.isInConfirmMatchProcess = (userId) => isInConfirmMatchProcess(userId);

exports.recommendPlaceForMatch = async (matchId) => {
    const match = await this.getMatchById(matchId);

    const { locations } = match;
    const latAverage = (locations[0].lat + locations[1].lat) / 2;
    const longAverage = (locations[0].long + locations[1].long) / 2;

    const latAdd = 0.02;
    const longAdd = 0.02;

    let count = 1;
    let places = [];
    while (places.length < 5 && count < 20) {
        places = (
            await getPlacesInViewPort({
                lat_left_top: latAverage + latAdd * count,
                lat_right_bottom: latAverage - latAdd * count,
                long_left_top: longAverage - longAdd * count,
                long_right_bottom: longAverage + longAdd * count,
            })
        ).places;
        count += 1;
    }

    if (count === 20) return places;
    return places.slice(0, 5);
};

const checkInConfirmProcess = async (userId) => {
    const check = await Match.findOne({
        participants: {
            $in: [userId],
        },
        status: 0,
    });
    if (check) {
        throw new AppError(400, 'Fail, you are in confirm process');
    }
};

exports.matchLater = async (userId, data) => {
    await checkInConfirmProcess(userId);

    const user = await getUserById(userId)
        .select('+favorites +occupations')
        .populate('favorites', '_id tags')
        .populate('occupations', '_id tags');

    return await createRequestMatch(user, data, 2);
};

// Cron job match later
cron.schedule('*/10 * * * * *', async () => {
    const matchs = await findListUserMatchedLater();
    for (const match of matchs) {
        const { user1, user2 } = match;
        const data1 = await getUserById(user1)
            .select('+favorites +occupations')
            .populate('favorites', 'name')
            .populate('occupations', 'name');
        const data2 = await getUserById(user2)
            .select('+favorites +occupations')
            .populate('favorites', 'name')
            .populate('occupations', 'name');
        const created = new Date().getTime();
        const matchCreated = await this.createMatch({
            created,
            participants: [user1, user2],
            locations: [
                {
                    lat: data1.lat,
                    long: data1.long,
                },
                {
                    lat: data2.lat,
                    long: data2.long,
                },
            ],
            timeout: created + parseInt(process.env.TIMEOUT_MATCH_LATER, 10),
            status: 0,
        });

        const notificationId = await sendNotification([user1, user2], '2', {
            serviceid: matchCreated._id,
            timeout: `${matchCreated.timeout}`,
            user1: data1,
            user2: data2,
        });

        setTimeout(async () => {
            await Match.findOneAndUpdate(
                {
                    _id: matchCreated._id.toString(),
                    status: 0,
                },
                {
                    status: -2,
                }
            );
            deleteNotification(notificationId);
        }, parseInt(process.env.TIMEOUT_MATCH_LATER, 10));
    }
});

exports.matchNow = async (userId, data) => {
    await checkInConfirmProcess(userId);

    const user = await getUserById(userId)
        .select('+favorites +occupations')
        .populate('favorites', 'name')
        .populate('occupations', 'name');
    const requestMatch = await createRequestMatch(user, data, 1);

    const picker = await findUserMatchNow(requestMatch);
    if (picker) {
        const created = new Date().getTime();
        const match = await this.createMatch({
            created,
            participants: [user.id, picker.user.id],
            locations: [
                {
                    lat: requestMatch.user.lat,
                    long: requestMatch.user.long,
                },
                {
                    lat: picker.user.lat,
                    long: picker.user.long,
                },
            ],
            timeout: created + parseInt(process.env.TIMEOUT_MATCH_NOW, 10),
            status: 0,
        });
        const notificationId = await sendNotification(
            [user.id, picker.user.id],
            '1',
            {
                serviceid: match._id,
                timeout: `${match.timeout}`,
                user1: user,
                user2: await getUserById(picker.user.id)
                    .select('+favorites +occupations')
                    .populate('favorites', 'name')
                    .populate('occupations', 'name'),
            }
        );

        setTimeout(async () => {
            await Match.findOneAndUpdate(
                {
                    _id: match._id.toString(),
                    status: 0,
                },
                {
                    status: -2,
                }
            );

            deleteNotification(notificationId);
        }, parseInt(process.env.TIMEOUT_MATCH_NOW, 10));
    }
};

exports.getMatchOfUserByMatchId = (userId, matchId) =>
    Match.findOne({
        _id: matchId,
        participants: { $in: userId },
        status: { $ne: -2 },
    });

exports.getMatchOfUser = async (userId) => {
    const match = await Match.find(
        {
            participants: { $in: userId },
            status: { $ne: -2 },
        },
        {
            messages: { $slice: -1 },
        }
    )
        .select('+messages')
        .populate('participants');

    return match;
};

exports.createMatch = (data) => {
    const { participants, status, timeout, created, locations } = data;
    const match = Match({
        created,
        timeout,
        participants,
        status,
        locations,
    });
    return match.save();
};

exports.storeChat = async (data) => {
    const { sender, matchId, message, type } = data;
    const match = await this.getMatchOfUserByMatchId(sender, matchId).select(
        '+messages'
    );
    if (!match)
        throw new AppError(404, 'Not found or user dont in participants!');

    const { messages } = match;
    const lastMessage = messages[messages.length - 1];
    if (lastMessage) {
        const timeDistance = new Date().getTime() - lastMessage.created;
        if (lastMessage.sender === sender && timeDistance < 30 * 1000) {
            lastMessage.data.push({ message, type });
            lastMessage.created = new Date().getTime();
            return Match.findByIdAndUpdate(matchId, { messages });
        }
    }

    messages.push({
        sender,
        data: [{ message, type }],
        created: new Date().getTime(),
        index: messages.length,
    });
    return Match.findByIdAndUpdate(matchId, { messages });
};

exports.getChatOfMatch = async (userId, data) => {
    const match = await this.getMatchOfUserByMatchId(
        userId,
        data.matchId
    ).select('+messages');
    if (!match)
        throw new AppError(404, 'Not found or user dont in participants!');

    let lastMessage;
    if (
        data.lastMessage &&
        parseInt(data.lastMessage, 10) < match.messages.length
    )
        lastMessage = parseInt(data.lastMessage, 10);
    else lastMessage = match.messages.length;

    const participants = {};
    for (const user of match.participants) {
        participants[user] = await getUserById(user);
    }

    const result = [];
    for (let index = lastMessage - 1; index >= lastMessage - 10; index -= 1) {
        if (index < 0) break;
        const messages = match.messages[index];
        const user = participants[messages.sender];
        result.push({
            ...messages,
            avatar: user.avatar,
            lastname: user.lastname,
            firstname: user.firstname,
        });
    }
    return result;
};

exports.notifyOfflineUser = async (users, matchId, data) => {
    const match = await this.getMatchById(matchId);
    const offlines = [];
    for (const participant of match.participants) {
        let offline = true;
        for (const user of users) {
            if (participant === user) {
                offline = false;
                break;
            }
        }
        if (offline) offlines.push(participant);
    }
    sendNotification(offlines, '7', data, null, false);
};

exports.confirmMatch = async (userId, data) => {
    const { id, status } = data;
    const notification = await getNotificationById(id);
    if (!notification) {
        throw new AppError(404, 'Not found, time out or some user deny');
    }

    const timeout = notification.data.timeout || null;
    if (!timeout) {
        throw new AppError(400, 'Dont found data');
    }
    if (new Date().getTime() > parseInt(timeout, 10)) {
        deleteNotification(id);
        throw new AppError(400, 'Time out');
    }

    const updateStatus = await updateNotification(id, userId, status, true);
    if (updateStatus === 2 || updateStatus === -1) {
        const update = await Match.findByIdAndUpdate(
            notification.serviceid,
            {
                status: updateStatus - 1,
            },
            { new: true }
        );
        sendNotification(
            notification.receiver,
            '3',
            {
                result: updateStatus === 2 ? '1' : '-1',
                matchId: updateStatus === 2 ? notification.serviceid : '',
            },
            null,
            false
        );
        deleteNotification(id);
        return update;
    }
};

exports.sendPlaceRequest = async (userid, data) => {
    const { matchid, placeid, date } = data;
    const match = await this.getMatchOfUserByMatchId(userid, matchid);
    if (!match)
        throw new AppError(
            404,
            'Match not found or user dont in participants!'
        );
    const place = await getPlaceById(placeid);
    if (!place) throw new AppError(404, 'Place not found');
    await sendNotification(
        match.participants,
        '8',
        { serviceid: matchid, place, date },
        userid
    );
    return true;
};

exports.confirmPlace = async (userid, data) => {
    const { status, notificationid } = data;
    const noti = await getNotificationById(notificationid);
    isObjectNull(noti);

    if (noti.sender === userid || !noti.receiver.includes(userid))
        throw new AppError(409, 'you are sender or not in request');

    const { date, place } = noti.data;
    await deleteNotification(notificationid);
    const statusMeet = status ? 0 : -1;

    const match = await this.getMatchById(noti.serviceid);
    isObjectNull(match);

    sendNotification(
        [noti.sender, userid],
        '10',
        { status, matchid: noti.serviceid },
        null,
        false
    );

    return await saveMeetingHistory(
        match.participants,
        noti.serviceid,
        date,
        place._id,
        statusMeet
    );
};

exports.deletePlaceRequest = async (userid, data) => {
    const { notificationid } = data;
    const noti = await getNotificationById(notificationid);
    isObjectNull(noti);

    if (noti.sender !== userid) throw new AppError(409, 'you are not sender');

    await deleteNotification(notificationid);
    return true;
};

exports.cancelRequestMatch = async (userId) => await cancelRequestMatch(userId);
