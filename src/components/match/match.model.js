const mongoose = require('mongoose');

const MatchSchema = new mongoose.Schema(
    {
        participants: {
            type: [
                {
                    type: String,
                    ref: 'User',
                },
            ],
            required: true,
            default: [],
        },
        locations: {
            type: [
                {
                    lat: { type: Number },
                    long: { type: Number },
                },
            ],
            required: true,
            default: [],
        },
        messages: {
            type: Array,
            required: true,
            default: [],
            select: false,
        },
        timeout: {
            type: Number,
        },
        created: {
            type: Number,
        },
        // 0: wating to confirm, 1: chating, 2 - met , -1: fail when chatting, -2: fail when countdown
        status: {
            type: Number,
        },
    },
    {
        collection: 'match',
    }
);

module.exports = mongoose.model('Match', MatchSchema);
