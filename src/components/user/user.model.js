const mongoose = require('mongoose');
const validator = require('validator');

const UserSchema = new mongoose.Schema(
    {
        username: {
            type: String,
            required: true,
            unique: true,
            minlength: 6,
            maxlength: 40,
        },
        email: {
            type: String,
            validate: [validator.isEmail, 'invalid email'],
        },
        phone: {
            type: String,
            minlength: 10,
            maxlength: 10,
        },
        password: {
            type: String,
            required: true,
            minlength: 6,
            maxlength: 128,
            select: false,
        },
        firstname: {
            type: String,
            required: true,
            minlength: 1,
            maxlength: 30,
        },
        lastname: {
            type: String,
            required: true,
            minlength: 1,
            maxlength: 30,
        },
        registerdate: {
            type: Number,
            default: new Date().getTime(),
        },
        dateofbirth: {
            type: Number,
            max: new Date().getTime(),
            default: 0,
        },
        // 0 - Unknow; 1 - Men; 2 - Women; 3 - Other
        gender: {
            type: Number,
            max: 3,
            min: 0,
            default: 0,
        },
        // 0 - Unknow; 1 - In relationship; 2 - Alone; 3 - Divorcee
        relationship: {
            type: Number,
            max: 3,
            min: 0,
            default: 0,
        },
        avatar: {
            type: Array,
            default: [
                'https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png',
            ],
        },
        lat: {
            type: Number,
            max: 90,
            min: -90,
        },
        long: {
            type: Number,
            max: 180,
            min: -180,
        },
        listliked: {
            type: Array,
            default: [],
            select: false,
        },
        historymatch: {
            type: Array,
            default: [],
            select: false,
        },
        friends: {
            type: Array,
            default: [],
            select: false,
        },
        occupations: {
            type: [
                {
                    type: String,
                    ref: 'Occupation',
                },
            ],
            default: [],
            select: false,
        },
        favorites: {
            type: [
                {
                    type: String,
                    ref: 'Favorite',
                },
            ],
            select: false,
            default: [],
        },
        // 1 - Non-Block; -1 - Block
        status: {
            type: Number,
            default: 1,
            select: false,
        },
    },
    {
        collection: 'user',
    }
);

module.exports = mongoose.model('User', UserSchema);
