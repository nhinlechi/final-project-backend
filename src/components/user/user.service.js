const bcrypt = require('bcryptjs');
const User = require('./user.model.js');
const AppError = require('../../utils/appError.js');

// Get user by id
exports.getUserById = (id) => User.findById(id);

// Get user by username
exports.getUserByUsername = (username) =>
    User.findOne({ username }).select('+password');

// Get user information
exports.getInformationUser = async (userId, select = []) => {
    let expose = '';
    select.forEach((field) => {
        expose += ` +${field}`;
    });
    const user = this.getUserById(userId).select(expose);
    if (expose.includes('favorites')) user.populate('favorites', '-tags -__v');
    if (expose.includes('occupations'))
        user.populate('occupations', '-tags -__v');
    return await user;
};

// Compare password
const comparePassword = async (password, hashPassword) => {
    const valid = await bcrypt.compare(password, hashPassword);
    if (!valid) throw new AppError(401, 'Old password is incorrect');
    return true;
};

// Hash password
const hashPassword = async (password) => {
    const salt = await bcrypt.genSalt(10);
    return bcrypt.hash(password, salt);
};

exports.resetPassword = async (email, password) =>
    await User.findOneAndUpdate(
        {
            $or: [{ username: email }, { email }],
        },
        { password: await hashPassword(password) }
    );

// Check username exist
exports.checkUsername = async (username) => {
    const usernameExist = await User.findOne({ username });
    if (usernameExist) {
        throw new AppError(409, 'Username exist');
    }
};

// Create user
exports.createUser = async (data) => {
    const { username, password, firstname, lastname } = data;
    await this.checkUsername(username);
    const hashPw = await hashPassword(password);
    const user = User({ username, password: hashPw, firstname, lastname });
    return await user.save();
};

// Update user
exports.updateUser = async (id, data) => {
    const user = await this.getUserById(id).select('+favorites +occupations');
    for (const [field, value] of Object.entries(data)) user[field] = value;
    return await User.findByIdAndUpdate(id, user, { new: true })
        .populate('favorites', '-tags -__v')
        .populate('occupations', '-tags -__v');
};

// Change password
exports.updatePasswordUser = async (id, data) => {
    const { password, newpassword } = data;
    const user = await this.getUserById(id).select('+password');
    await comparePassword(password, user.password);
    user.password = await hashPassword(newpassword);
    await User.findByIdAndUpdate(id, user);
};

// Add information extentions
exports.addInformationExtention = async (idUser, data) => {
    const expose = ' +friends +historymatch +listliked';
    const user = await this.getUserById(idUser).select(expose);
    for (const [field, values] of Object.entries(data)) {
        const temp = values.concat(user[field]);
        user[field] = [...new Set(temp)];
    }
    return await User.findByIdAndUpdate(idUser, user, { new: true }).select(
        expose
    );
};

// Remove information extentions
exports.removeInformationExtention = async (idUser, data) => {
    const expose = ' +friends +historymatch +listliked';
    const user = await this.getUserById(idUser).select(expose);
    for (const [field, values] of Object.entries(data)) {
        user[field] = user[field].filter((id) => {
            for (const value of values) {
                if (id === value) return false;
            }
            return true;
        });
    }
    return await User.findByIdAndUpdate(idUser, user, { new: true }).select(
        expose
    );
};
