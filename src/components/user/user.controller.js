const { catchAsync } = require('../../utils/catchAsync');
const { QUERY, UPDATE, CREATE } = require('../../middlewares/type-response');
const { successResponse } = require('../../middlewares/success-response');
const {
    selectFieldUserValidation,
    updateUserValidation,
    changePasswordValidation,
    saveFirebaseTokenValidation,
    updateUserExtensionValidation,
    forgotPasswordValidation,
    resetNewPasswordValidation,
} = require('../../validations/user.validation.js');
const {
    getInformationUser,
    updateUser,
    updatePasswordUser,
    saveFirebaseToken,
    addInformationExtention,
    removeInformationExtention,
} = require('./user.service');
const {
    forgotPassword,
    resetNewPassword,
} = require('../forgotpassword/forgotpassword.service');

const { idValidation } = require('../../validations/common.validation');

exports.forgotPassword = catchAsync(async (req, res) => {
    await forgotPasswordValidation(req.body);
    const forgot = await forgotPassword(req.body);
    successResponse(res, CREATE, forgot);
});

exports.resetNewPassword = catchAsync(async (req, res) => {
    await resetNewPasswordValidation(req.body);
    const reset = await resetNewPassword(req.body);
    successResponse(res, UPDATE, reset);
});

// Get my information
exports.getMyInformation = catchAsync(async (req, res) => {
    await selectFieldUserValidation(req.query);
    const user = await getInformationUser(req.user._id, req.query.select);
    successResponse(res, QUERY, user);
});

// Get user information
exports.getUserInformation = catchAsync(async (req, res) => {
    await idValidation({ id: req.params.id });
    await selectFieldUserValidation(req.query);
    const user = await getInformationUser(req.params.id, req.query.select);
    successResponse(res, QUERY, user);
});

// Update user
exports.updateUser = catchAsync(async (req, res) => {
    await updateUserValidation(req.body);
    const update = await updateUser(req.user._id, req.body);
    successResponse(res, UPDATE, update);
});

// Add information extension
exports.addInformationExtension = catchAsync(async (req, res) => {
    await updateUserExtensionValidation(req.body);
    const update = await addInformationExtention(req.user._id, req.body);
    successResponse(res, UPDATE, update);
});

// remove information extension
exports.removeInformationExtension = catchAsync(async (req, res) => {
    await updateUserExtensionValidation(req.body);
    const update = await removeInformationExtention(req.user._id, req.body);
    successResponse(res, UPDATE, update);
});

// Change password
exports.changePassword = catchAsync(async (req, res) => {
    await changePasswordValidation(req.body);
    await updatePasswordUser(req.user._id, req.body);
    successResponse(res, UPDATE);
});

// Save firebase Token
exports.saveFirebaseToken = catchAsync(async (req, res) => {
    await saveFirebaseTokenValidation(req.body);
    await saveFirebaseToken(req.user._id, req.body);
    successResponse(res, UPDATE);
});
