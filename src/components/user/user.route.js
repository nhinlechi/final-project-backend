const router = require('express').Router();
const userController = require('./user.controller.js');
const { verifyToken } = require('../../middlewares/authentication');

router.post('/forgotpassword', userController.forgotPassword);
router.put('/resetnewpassword', userController.resetNewPassword);
router.use(verifyToken);
router.get('/', userController.getMyInformation);
router.get('/:id', userController.getUserInformation);
router.patch('/update', userController.updateUser);
router.patch(
    '/addinformationextension',
    userController.addInformationExtension
);
router.patch(
    '/removeinformationextension',
    userController.removeInformationExtension
);
router.put('/changepassword', userController.changePassword);

module.exports = router;
