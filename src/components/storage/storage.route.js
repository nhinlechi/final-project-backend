const router = require('express').Router();
const storageController = require('./storage.controller');

router.get('/', storageController.resignedURLStorage);

module.exports = router;
