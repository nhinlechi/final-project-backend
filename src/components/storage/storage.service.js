const { Storage } = require('@google-cloud/storage');
const { v4: uuidv4 } = require('uuid');
const AppError = require('../../utils/appError.js');

// Creates a client
const storage = new Storage({
    projectId: 'together-6966d',
    keyFilename: 'src/components/storage/googleCloudServiceAccount.json',
});
const bucketname = 'final-project-togethers-app.appspot.com';

const fileNameExits = async (filename) => {
    const bucket = storage.bucket(bucketname);
    const file = bucket.file(filename);
    const exits = await file.exists();
    return exits[0];
};

exports.generateV4UploadSignedUrl = async (data) => {
    const { type } = data;
    let name = uuidv4();
    if (await fileNameExits(name))
        throw new AppError(409, ' file name already exits');

    const options = {
        version: 'v4',
        action: 'write',
        expires: Date.now() + 15 * 60 * 1000, // 15 minutes
        contentType: type,
    };

    // Get a v4 signed URL for uploading file
    const [url] = await storage
        .bucket(bucketname)
        .file(name)
        .getSignedUrl(options);
    name = `https://storage.googleapis.com/${bucketname}/${name}`;

    return { uploadurl: url, accessurl: name };
};
