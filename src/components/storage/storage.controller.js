const { catchAsync } = require('../../utils/catchAsync');
const { QUERY } = require('../../middlewares/type-response');
const { successResponse } = require('../../middlewares/success-response');
const { generateV4UploadSignedUrl } = require('./storage.service');
const {
    resignedURLStorageValidation,
} = require('../../validations/storage.validate');

exports.resignedURLStorage = catchAsync(async (req, res) => {
    await resignedURLStorageValidation(req.query);
    const resigned = await generateV4UploadSignedUrl(req.query);
    successResponse(res, QUERY, resigned);
});
