const Notification = require('./notification.model');
const AppError = require('../../utils/appError.js');
const { sendNotification } = require('../firebase/firebase.service');
const { getTittle } = require('./notification-code');

exports.isInConfirmMatchProcess = async (userId) => {
    const confirmProcess = await Notification.findOne({
        $or: [{ code: 1 }, { code: 2 }],
        receiver: {
            $in: [userId],
        },
    });
    return confirmProcess ? confirmProcess.code : -1;
};

exports.getNotificationById = (id) =>
    Notification.findById(id).select('+receiver +status +sender');

exports.deleteNotification = async (id) =>
    await Notification.findByIdAndDelete(id);

exports.getAllNotification = async (userid, data) => {
    const { page, rowsperpage, ...query } = data;
    query.receiver = userid;

    const total = await Notification.countDocuments(query);
    const notification = await Notification.find(query)
        .limit(rowsperpage)
        .skip((page - 1) * rowsperpage)
        .sort({ created: -1 });

    return { total, notification };
};

exports.saveNotification = async (
    sender,
    receiver,
    code,
    serviceid,
    notiData
) => {
    const status = [];
    for (let i = 0; i < receiver.length; i += 1) status.push(false);
    const data = { ...notiData };
    delete data.serviceid;
    const notification = Notification({
        sender,
        receiver,
        code,
        serviceid,
        data,
        status,
    });
    return await notification.save();
};

// check internal Notification Exits base on require of service
exports.checkNotificationExits = async (receiver, sender, code, serviceid) => {
    const notification = await Notification.findOne({
        receiver,
        sender,
        code,
        serviceid,
    });
    if (notification) throw new AppError(409, 'Duplicate request');
};

// receiver is array
exports.sendNotification = async (
    receiver,
    code,
    data,
    sender = null,
    save = true
) => {
    let sendData = data;
    if (save) {
        const noti = await this.saveNotification(
            sender,
            receiver,
            code,
            data.serviceid,
            data
        );
        sendData = Object.assign(
            data,
            { notificationid: String(noti._id) },
            sender === null ? null : { sender }
        );
        for (let i = 0; i < receiver.length; i += 1) {
            await sendNotification(
                receiver[i],
                getTittle(code),
                code,
                sendData
            );
        }
        return noti._id;
    }
    for (let i = 0; i < receiver.length; i += 1) {
        await sendNotification(receiver[i], getTittle(code), code, sendData);
    }
};

// Return:
// -1: Update status false and delete
// 1: Update status false but dont delete
// 2: Update true, all user turn on "true" flag,
// 3: Update true, some user have "fasle" flag
exports.updateNotification = async (
    id,
    userid,
    status,
    deleteNotify = false
) => {
    const notification = await Notification.findOne({
        _id: id,
        receiver: {
            $in: userid,
        },
    }).select('+receiver +status');
    if (!notification) {
        throw new AppError(404, 'Not found or user dont in notification');
    }

    notification.status[notification.receiver.indexOf(userid)] = status;

    if (status) {
        await Notification.findByIdAndUpdate(id, notification);
        const checkAllUserConfirm = await this.checkStatus(id);
        return checkAllUserConfirm ? 2 : 3;
    }
    if (deleteNotify) {
        await this.deleteNotification(id);
        return -1;
    }
    await Notification.findByIdAndUpdate(id, notification);
    return 1;
};

// If all "true" flag return true
exports.checkStatus = async (id) => {
    const notification = await Notification.findOne({
        _id: id,
        status: {
            $nin: [false],
        },
    });
    return !!notification;
};
