const router = require('express').Router();
const notificationController = require('./notification.controller');

router.get('/', notificationController.getAllNotification);

module.exports = router;
