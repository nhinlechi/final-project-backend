const { catchAsync } = require('../../utils/catchAsync');
const { QUERY } = require('../../middlewares/type-response');
const { successResponse } = require('../../middlewares/success-response');
const { getAllNotification } = require('./notication.service');

const {
    getAllNotificationValidation,
} = require('../../validations/notification.validate');

exports.getAllNotification = catchAsync(async (req, res) => {
    await getAllNotificationValidation(req.query);
    const listNotification = await getAllNotification(req.user._id, req.query);
    successResponse(res, QUERY, listNotification);
});
