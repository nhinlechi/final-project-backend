const mongoose = require('mongoose');

const NotificationSchema = new mongoose.Schema(
    {
        sender: {
            type: String,
            default: null,
        },
        receiver: {
            type: Array,
            required: true,
            select: false,
        },
        code: {
            type: Number,
            required: true,
        },
        // serviceid can be matchid, groupid , etc,..
        serviceid: {
            type: String,
            default: null,
        },
        data: {
            type: Object,
            default: {},
        },
        status: {
            type: Array,
            default: [false],
            select: false,
        },
        created: {
            type: Number,
            default: new Date().getTime(),
        },
    },
    {
        collection: 'notification',
    }
);

module.exports = mongoose.model('Notification', NotificationSchema);
