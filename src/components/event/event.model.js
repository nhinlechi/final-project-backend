const mongoose = require('mongoose');

const eventSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
        },
        description: {
            type: String,
        },
        participants: {
            type: [
                {
                    type: String,
                    ref: 'User',
                },
            ],
            required: true,
            default: [],
        },
        groupid: {
            type: String,
            required: true,
            ref: 'Group',
        },
        placeid: {
            type: String,
            required: true,
            ref: 'Place',
        },
        date: {
            type: Number,
            required: true,
        },
        messages: {
            type: Array,
            required: true,
            default: [],
            select: false,
        },
        status: {
            // 0: up comming, 1: gone , -1: cancel
            type: Number,
            default: 0,
        },
    },
    {
        collection: 'event',
    }
);

module.exports = mongoose.model('event', eventSchema);
