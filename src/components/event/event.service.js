const cron = require('node-cron');
const Event = require('./event.model');
const AppError = require('../../utils/appError.js');
const { isObjectNull } = require('../../common/error.detection');
const {
    getGroupById,
    getGroupByIdnotPopulate,
} = require('../group/group.service');
const { sendNotification } = require('../notification/notication.service');
const { getUserById } = require('../user/user.service');

const updateEventInternal = async (event) => {
    event = await Event.findByIdAndUpdate(event._id, event, {
        new: true,
    })
        .populate('placeid')
        .populate('participants', 'avatar firstname');
    return event;
};

exports.getEventById = async (id) =>
    await Event.findOne({ _id: id, status: { $ne: -1 } });

exports.getEvent = async (data) => {
    const { id, groupid } = data;
    return await Event.findOne({ _id: id, status: { $ne: -1 }, groupid })
        .populate('placeid')
        .populate('participants', 'avatar firstname');
};
exports.createEvent = async (userid, data) => {
    const { name, description, date, placeid, groupid } = data;
    let event = Event({
        participants: [userid],
        name,
        description,
        date,
        placeid,
        groupid,
    });
    event = await event.save();
    event = await Event.findById(event._id)
        .populate('placeid')
        .populate('participants', 'avatar firstname');
    return event;
};

exports.getAllEvent = async (data) => {
    const group = await getGroupById(data.groupid);
    isObjectNull(group);
    const { page, rowsperpage, ...query } = data;
    if (!query.status) {
        query.status = { $ne: -1 };
    }
    const total = await Event.countDocuments(query);
    const events = await Event.find(query)
        .sort({ date: -1 })
        .limit(rowsperpage)
        .skip((page - 1) * rowsperpage)
        .populate('placeid')
        .populate('participants', 'avatar firstname');
    return { total, events };
};

exports.updateEvent = async (data) => {
    const event = await this.getEventById(data.eventid);
    isObjectNull(event);
    for (const [field, value] of Object.entries(data)) event[field] = value;
    return await Event.findByIdAndUpdate(data.eventid, event, { new: true })
        .populate('placeid')
        .populate('participants', 'avatar firstname');
};

exports.deleteEvent = async (data) => {
    const event = await this.getEventById(data.eventid);
    isObjectNull(event);
    event.status = -1;
    return await Event.findByIdAndUpdate(data.eventid, event);
};

exports.joinEvent = async (userid, data) => {
    const { status, eventid } = data;
    const event = await this.getEventById(eventid);
    isObjectNull(event);
    if (status) {
        if (event.participants.includes(userid))
            throw new AppError(409, 'This user aready join');
        event.participants.push(userid);
    } else {
        if (!event.participants.includes(userid))
            throw new AppError(409, 'This user have not join yet');
        const participantIndex = event.participants.indexOf(userid);
        event.participants.splice(participantIndex, 1);
    }
    return await Event.findByIdAndUpdate(data.eventid, event, { new: true })
        .populate('placeid')
        .populate('participants', 'avatar firstname');
};

exports.storeChat = async (data) => {
    const { sender, eventid, message, type } = data;
    const event = await Event.findById(eventid).select('+messages');
    if (!event)
        throw new AppError(404, 'Not found or user dont in participants!');

    const { messages } = event;
    const lastMessage = messages[messages.length - 1];
    if (lastMessage) {
        const timeDistance = new Date().getTime() - lastMessage.created;
        if (lastMessage.sender === sender && timeDistance < 30 * 1000) {
            lastMessage.data.push({ message, type });
            lastMessage.created = new Date().getTime();
            return Event.findByIdAndUpdate(eventid, { messages });
        }
    }

    messages.push({
        sender,
        data: [{ message, type }],
        created: new Date().getTime(),
        index: messages.length,
    });
    return Event.findByIdAndUpdate(eventid, { messages });
};

exports.getChatOfEvent = async (data) => {
    const { eventid, groupid } = data;
    const event = await Event.findById(eventid).select('+messages');
    const group = await getGroupByIdnotPopulate(groupid);
    if (!event || !group)
        throw new AppError(404, 'Not found or user dont in participants!');

    let lastMessage;
    if (
        data.lastMessage &&
        parseInt(data.lastMessage, 10) < event.messages.length
    )
        lastMessage = parseInt(data.lastMessage, 10);
    else lastMessage = event.messages.length;

    const participants = {};
    for (const user of group.participant) {
        participants[user] = await getUserById(user);
    }

    const result = [];
    for (let index = lastMessage - 1; index >= lastMessage - 10; index -= 1) {
        if (index < 0) break;
        const messages = event.messages[index];
        const user = participants[messages.sender];
        if (user) {
            result.push({
                ...messages,
                avatar: user.avatar,
                lastname: user.lastname,
                firstname: user.firstname,
            });
        }
    }
    return result;
};

cron.schedule('*/30 * * * *', async () => {
    // notify up comming events
    const current = Date.now();
    const events = await Event.find({
        date: {
            $gte: current,
            $lt: current + 1000 * 3599, // 1 hour
        },
    });
    for (let i = 0; i < events.length; i += 1) {
        const durationInMinute = (events[i].date - current) / (1000 * 60);
        sendNotification(
            events[i].participants,
            '6',
            { eventid: events[i]._id, durationInMinute },
            null,
            false
        );
    }

    // find and update status of event
    const goneEvents = await Event.find({
        date: {
            $lt: current,
        },
        status: 0,
    });

    for (let i = 0; i < goneEvents.length; i += 1) {
        goneEvents[i].status = 1;
        updateEventInternal(goneEvents[i]);
    }
});
