const router = require('express').Router({ mergeParams: true });
const eventController = require('./event.controller.js');

router.get('/', eventController.getAllEvent);
router.post('/', eventController.createEvent);
router.patch('/', eventController.updateEvent);
router.delete('/', eventController.deleteEvent);
router.post('/join', eventController.joinEvent);
router.get('/chat', eventController.getChat);
router.get('/:id', eventController.getEvent);

module.exports = router;
