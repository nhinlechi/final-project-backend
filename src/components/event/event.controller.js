const { catchAsync } = require('../../utils/catchAsync');
const {
    QUERY,
    CREATE,
    UPDATE,
    DELETE,
} = require('../../middlewares/type-response');
const { successResponse } = require('../../middlewares/success-response');
const {
    createEventValidation,
    getEventValidation,
    updateEventValidation,
    deleteEventValidation,
    joinEventValidation,
    getChatValidation,
    getEventDetailValidation,
} = require('../../validations/event.validation');
const { isGroupHostId, isGroupMemmberId } = require('../group/group.service');
const {
    createEvent,
    getAllEvent,
    getEvent,
    updateEvent,
    deleteEvent,
    joinEvent,
    getChatOfEvent,
} = require('./event.service');

exports.getAllEvent = catchAsync(async (req, res) => {
    req.query = Object.assign(req.query, req.params);
    await getEventValidation(req.query);
    const event = await getAllEvent(req.query);
    successResponse(res, QUERY, event);
});

exports.getEvent = catchAsync(async (req, res) => {
    await getEventDetailValidation(req.params);
    const event = await getEvent(req.params);
    successResponse(res, QUERY, event);
});

exports.createEvent = catchAsync(async (req, res) => {
    req.body = Object.assign(req.body, req.params);
    await createEventValidation(req.body);
    await isGroupHostId(req.params.groupid, req.user._id);
    const event = await createEvent(req.user._id, req.body);
    successResponse(res, CREATE, event);
});

exports.deleteEvent = catchAsync(async (req, res) => {
    await deleteEventValidation(Object.assign(req.body, req.params));
    await isGroupHostId(req.params.groupid, req.user._id);
    const event = await deleteEvent(req.body);
    successResponse(res, DELETE, event);
});

exports.updateEvent = catchAsync(async (req, res) => {
    await updateEventValidation(Object.assign(req.body, req.params));
    await isGroupHostId(req.params.groupid, req.user._id);
    const event = await updateEvent(req.body);
    successResponse(res, UPDATE, event);
});

exports.joinEvent = catchAsync(async (req, res) => {
    await joinEventValidation(Object.assign(req.body, req.params));
    await isGroupMemmberId(req.params.groupid, req.user._id);
    const event = await joinEvent(req.user._id, req.body);
    successResponse(res, UPDATE, event);
});

exports.getChat = catchAsync(async (req, res) => {
    req.query = Object.assign(req.query, req.params);
    await getChatValidation(Object.assign(req.query));
    await isGroupMemmberId(req.params.groupid, req.user._id);
    const chat = await getChatOfEvent(req.query);
    successResponse(res, QUERY, chat);
});
