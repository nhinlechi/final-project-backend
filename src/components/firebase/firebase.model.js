const mongoose = require('mongoose');

const FirebaseSchema = new mongoose.Schema(
    {
        userid: {
            type: String,
            required: true,
            unique: true,
        },
        tokenid: {
            type: String,
            required: true,
        },
    },
    {
        collection: 'firebase',
    }
);

module.exports = mongoose.model('Firebase', FirebaseSchema);
