const admin = require('firebase-admin');
const Firebase = require('./firebase.model');
const serviceAccount = require('./config-firebase.json');
const AppError = require('../../utils/appError.js');
const logger = require('../../utils/logger');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://final-project-togethers-app.firebaseio.com',
});

const getUserFirebaseToken = async (userid) =>
    await Firebase.findOne({ userid });

exports.saveUserToken = async (userid, data) => {
    const { tokenid } = data;
    if (!tokenid) {
        throw new AppError(400, 'you have to add valid firebase token');
    }
    return await Firebase.findOneAndUpdate(
        { userid },
        { tokenid },
        { upsert: true, new: true }
    );
};

exports.sendNotification = async (
    userid,
    title,
    noticode,
    data = { result: '1' }
) => {
    const firebase = await getUserFirebaseToken(userid);
    if (!firebase) {
        throw new AppError(409, 'have not save firebase token yet');
    }
    const stringdata = JSON.stringify(data);
    const message = {
        notification: {
            title,
            body: noticode,
        },
        data: { stringdata },
        token: firebase.tokenid,
    };
    admin
        .messaging()
        .send(message)
        .then((response) => {
            logger.info('Successfully sent message:', response);
            return true;
        })
        .catch((error) => {
            logger.error('Error sending message:', error);
        });
};
