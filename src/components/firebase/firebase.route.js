const router = require('express').Router();
const firebaseController = require('./firebase.controller');

router.post('/', firebaseController.saveUserToken);

module.exports = router;
