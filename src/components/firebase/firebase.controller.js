const { catchAsync } = require('../../utils/catchAsync');
const { CREATE } = require('../../middlewares/type-response');
const { successResponse } = require('../../middlewares/success-response');
const { saveUserToken } = require('./firebase.service');

exports.saveUserToken = catchAsync(async (req, res) => {
    const newInfor = await saveUserToken(req.user._id, req.body);
    successResponse(res, CREATE, newInfor);
});
