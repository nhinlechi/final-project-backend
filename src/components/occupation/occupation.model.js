const mongoose = require('mongoose');

const OccupationSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
            unique: true,
            minlength: 1,
            maxlength: 30,
        },
        tags: {
            type: Array,
            required: true,
        },
    },
    {
        collection: 'occupation',
    }
);

module.exports = mongoose.model('Occupation', OccupationSchema);
