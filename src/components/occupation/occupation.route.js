const router = require('express').Router();
const occupationController = require('./occupation.controller.js');

router.get('/', occupationController.getAllOccupation);

module.exports = router;
