const Occupation = require('./occupation.model.js');

exports.getOccupationTags = (occupations) => {
    const occupationTags = [];
    for (const occupation of occupations) {
        occupationTags.push(occupation.tags);
    }
    return [...new Set([].concat(...occupationTags))];
};

// Get all Occupation
exports.getAllOccupation = async () => {
    const occupations = await Occupation.find();
    return occupations.map((occupation) => ({
        _id: occupation._id,
        name: occupation.name,
    }));
};
