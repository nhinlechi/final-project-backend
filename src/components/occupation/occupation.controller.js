const { catchAsync } = require('../../utils/catchAsync');
const { QUERY } = require('../../middlewares/type-response');
const { successResponse } = require('../../middlewares/success-response');
const { getAllOccupation } = require('./occupation.service');

// Get all Occupation
exports.getAllOccupation = catchAsync(async (req, res) => {
    const occupations = await getAllOccupation();
    successResponse(res, QUERY, occupations);
});
