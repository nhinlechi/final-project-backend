const RequestMatch = require('./request-match.model');
const AppError = require('../../utils/appError.js');
const { distanceBetweenTwoPlace } = require('../place/place.service');
const { getFavoriteTags } = require('../favorite/favorite.service');
const { getOccupationTags } = require('../occupation/occupation.service');

const _calculateAge = (birthday) => {
    const ageDifMs = new Date().getTime() - birthday;
    const ageDate = new Date(ageDifMs);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
};

exports.cancelRequestMatch = async (userId) => {
    const cancel = await RequestMatch.findOneAndUpdate(
        {
            'user.id': userId,
            status: {
                $ne: -1,
            },
        },
        {
            status: -1,
        },
        {
            new: true,
        }
    );
    return cancel;
};

const isCanCreateRequestMatch = (user) => {
    if (!user.gender) {
        throw new AppError(400, 'Please update gender');
    }
    if (!user.relationship) {
        throw new AppError(400, 'Please update relationship');
    }
};

exports.createRequestMatch = async (user, data, status) => {
    isCanCreateRequestMatch(user);
    const create = await RequestMatch.findOneAndUpdate(
        {
            'user.id': user._id,
        },
        {
            status,
            user: {
                lat: data.lat,
                long: data.long,
                id: user._id,
                gender: user.gender,
                relationship: user.relationship,
                age: _calculateAge(user.dateofbirth),
                favorites: user.favorites,
                occupations: user.occupations,
            },
            criteria: {
                distance: 10,
                maxage: 100,
                minage: 18,
                ...data,
            },
            created: new Date().getTime(),
        },
        {
            upsert: true,
            new: true,
        }
    );
    return create;
};

const pickUserSatisfyDistance = (lat1, long1, requestMatchs, distance1) => {
    if (!requestMatchs.length) return null;
    for (const requestMatch of requestMatchs) {
        const { lat, long } = requestMatch.user;
        const { distance } = requestMatch.criteria;
        const distance2place = distanceBetweenTwoPlace(lat1, long1, lat, long);
        if (distance2place <= distance1 && distance2place <= distance)
            return requestMatch;
    }
    return null;
};

const updateStatusAndPush2Dismiss = (userId1, userId2) =>
    RequestMatch.findOneAndUpdate(
        {
            'user.id': userId1,
        },
        {
            status: -1,
            // $push: {
            //     dismiss: userId2,
            // },
        },
        {
            new: true,
        }
    );

const updateRequestMatchAfterMatched = async (userId1, userId2) => {
    await updateStatusAndPush2Dismiss(userId1, userId2);
    await updateStatusAndPush2Dismiss(userId2, userId1);
};

exports.findUserMatchNow = async (requestMatch) => {
    const { user, criteria, status } = requestMatch;
    const query = {
        status,
        'user.id': {
            $ne: user.id,
        },
        'user.age': {
            $lte: criteria.maxage,
            $gte: criteria.minage,
        },
        dismiss: {
            $nin: [user.id.toString()],
        },
        $and: [
            {
                $or: [
                    { 'criteria.gender': null },
                    { 'criteria.gender': user.gender },
                ],
            },
            {
                $or: [
                    { 'criteria.relationship': null },
                    { 'criteria.relationship': user.relationship },
                ],
            },
        ],
        'criteria.maxage': {
            $gte: user.age,
        },
        'criteria.minage': {
            $lte: user.age,
        },
    };
    if (criteria.gender) query['user.gender'] = criteria.gender;
    if (criteria.relationship)
        query['user.relationship'] = criteria.relationship;

    const requestMatchs = await RequestMatch.find(query).sort({
        created: 1,
    });
    const picker = pickUserSatisfyDistance(
        user.lat,
        user.long,
        requestMatchs,
        criteria.distance
    );
    if (picker)
        await updateRequestMatchAfterMatched(
            user.id.toString(),
            picker.user.id.toString()
        );
    return picker;
};

const countEqualElementInArray = (array1, array2) => {
    let count = 0;
    for (const element of array1) {
        count += array2.includes(element) ? 1 : 0;
    }
    return count;
};

const caculaterPointFavorite = (favorites1, favorites2) => {
    const favoritesId1 = favorites1.map((favorite) => favorite._id);
    const favoritesId2 = favorites2.map((favorite) => favorite._id);

    const point = countEqualElementInArray(favoritesId1, favoritesId2);
    if (point) return point * 3;

    const favoritesTag1 = getFavoriteTags(favorites1);
    const favoritesTag2 = getFavoriteTags(favorites2);

    return countEqualElementInArray(favoritesTag1, favoritesTag2);
};

const caculaterPointOccupation = (occupations1, occupations2) => {
    const occupationsId1 = occupations1.map((occupation) => occupation._id);
    const occupationsId2 = occupations2.map((occupation) => occupation._id);

    const point = countEqualElementInArray(occupationsId1, occupationsId2);
    if (point) return point * 3;

    const occupationsTag1 = getOccupationTags(occupations1);
    const occupationsTag2 = getOccupationTags(occupations2);

    return countEqualElementInArray(occupationsTag1, occupationsTag2);
};

const calculationPoint = (request1, request2) => {
    let point = 0;
    const user1 = request1.user;
    const criteria1 = request1.criteria;
    const user2 = request2.user;
    const criteria2 = request2.criteria;

    const distanceTwoPlace = distanceBetweenTwoPlace(
        user1.lat,
        user1.long,
        user2.lat,
        user2.long
    );

    if (
        // Gender
        (criteria2.gender && user1.gender !== criteria2.gender) ||
        (criteria1.gender && user2.gender !== criteria1.gender) ||
        // Relationship
        (criteria2.relationship &&
            user1.relationship !== criteria2.relationship) ||
        (criteria1.relationship &&
            user2.relationship !== criteria1.relationship) ||
        // Age
        user1.age < criteria2.minage ||
        user1.age > criteria2.maxage ||
        user2.age < criteria1.minage ||
        user2.age > criteria1.maxage ||
        // Distance
        distanceTwoPlace > criteria1.distance ||
        distanceTwoPlace > criteria2.distance
    )
        return -1;

    // Favorite
    point += caculaterPointFavorite(user1.favorites, user2.favorites);

    // Occupation
    point += caculaterPointOccupation(user1.occupations, user2.occupations);

    // Distance
    return point;
};

const calculationPointsOfUser = (requestUser, requestMatchs) =>
    requestMatchs.map((request) => ({
        id: request.user.id,
        point: calculationPoint(requestUser, request),
    }));

const culculationPointsOfAllUser = (requestMatchs) => {
    const pointsOfAllUser = {};
    for (let index = 0; index < requestMatchs.length; index += 1) {
        pointsOfAllUser[requestMatchs[index].user.id] = {
            status: 1,
            points: calculationPointsOfUser(
                requestMatchs[index],
                requestMatchs.slice(index + 1, requestMatchs.length)
            ),
        };
    }
    return pointsOfAllUser;
};

const comparePoint = (matchs, pointMatch, pointsOfAllUser, greater = false) => {
    for (const key in pointsOfAllUser) {
        if (pointsOfAllUser[key].status !== 1) continue;
        const { points } = pointsOfAllUser[key];
        for (const point of points) {
            if (
                point.point === pointMatch ||
                (greater && point.point > pointMatch)
            ) {
                matchs.push({
                    user1: key,
                    user2: point.id,
                });

                pointsOfAllUser[key].status = -1;
                pointsOfAllUser[point.id].status = -1;
                break;
            }
        }
    }
};

const matchLater = (pointsOfAllUser) => {
    const rangePointHigh = parseInt(process.env.RANGE_POINT_HIGH, 10);
    const rangePoint = parseInt(process.env.RANGE_POINT, 10);
    const matchs = [];

    comparePoint(matchs, rangePointHigh, pointsOfAllUser, true);

    for (
        let pointMatch = rangePointHigh - 1;
        pointMatch >= rangePoint;
        pointMatch -= 1
    ) {
        comparePoint(matchs, pointMatch, pointsOfAllUser);
    }

    return matchs;
};

exports.findListUserMatchedLater = async () => {
    const requestMatchs = await RequestMatch.find({
        status: 2,
    })
        .populate('user.favorites')
        .populate('user.occupations');

    const pointsOfAllUser = culculationPointsOfAllUser(requestMatchs);
    const matchs = matchLater(pointsOfAllUser);

    for (const match of matchs) {
        const { user1, user2 } = match;
        await updateRequestMatchAfterMatched(user1, user2);
    }

    return matchs;
};
