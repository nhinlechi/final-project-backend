const mongoose = require('mongoose');

const RequestMatchSchema = new mongoose.Schema(
    {
        user: {
            id: {
                type: String,
            },
            age: {
                type: Number,
                max: 100,
                min: 16,
            },
            lat: {
                type: Number,
                max: 90,
                min: -90,
            },
            long: {
                type: Number,
                max: 180,
                min: -180,
            },
            // 0 - Unknow; 1 - Men; 2 - Women; 3 - Other
            gender: {
                type: Number,
                max: 3,
                min: 0,
                default: 0,
            },
            // 0 - Unknow; 1 - In relationship; 2 - Alone; 3 - Divorcee
            relationship: {
                type: Number,
                max: 3,
                min: 0,
                default: 0,
            },
            favorites: {
                type: [
                    {
                        type: String,
                        ref: 'Favorite',
                    },
                ],
                default: [],
            },
            occupations: {
                type: [
                    {
                        type: String,
                        ref: 'Occupation',
                    },
                ],
                default: [],
            },
        },
        criteria: {
            distance: {
                type: Number,
                default: 10,
            },
            // 0 - Unknow; 1 - Men; 2 - Women; 3 - Other
            gender: {
                type: Number,
                max: 3,
                min: 0,
                default: 0,
            },
            // 0 - Unknow; 1 - In relationship; 2 - Alone; 3 - Divorcee
            relationship: {
                type: Number,
                max: 3,
                min: 0,
                default: 0,
            },
            maxage: {
                type: Number,
                max: 100,
                min: 16,
            },
            minage: {
                type: Number,
                max: 100,
                min: 16,
            },
        },
        dismiss: {
            type: Array,
            default: [],
        },
        created: {
            type: Number,
        },
        // -1: turn off request, 1: match now, 2: match later
        status: {
            type: Number,
        },
    },
    {
        collection: 'requestmatch',
    }
);

module.exports = mongoose.model('RequestMatch', RequestMatchSchema);
