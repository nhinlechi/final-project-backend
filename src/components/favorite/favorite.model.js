const mongoose = require('mongoose');

const FavoriteSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
            unique: true,
            minlength: 1,
            maxlength: 30,
        },
        tags: {
            type: Array,
            required: true,
        },
    },
    {
        collection: 'favorite',
    }
);

module.exports = mongoose.model('Favorite', FavoriteSchema);
