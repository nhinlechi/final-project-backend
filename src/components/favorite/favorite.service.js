const Favorite = require('./favorite.model.js');

exports.getFavoriteTags = (favorites) => {
    const favoriteTags = [];
    for (const favorite of favorites) {
        favoriteTags.push(favorite.tags);
    }
    return [...new Set([].concat(...favoriteTags))];
};

// Get all favorite
exports.getAllFavorite = async () => {
    const favorites = await Favorite.find();
    return favorites.map((favorite) => ({
        _id: favorite._id,
        name: favorite.name,
    }));
};
