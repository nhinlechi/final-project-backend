const { catchAsync } = require('../../utils/catchAsync');
const { QUERY } = require('../../middlewares/type-response');
const { successResponse } = require('../../middlewares/success-response');
const { getAllFavorite } = require('./favorite.service');

// Get all favorite
exports.getAllFavorite = catchAsync(async (req, res) => {
    const favorites = await getAllFavorite();
    successResponse(res, QUERY, favorites);
});
