const router = require('express').Router();
const favoriteController = require('./favorite.controller.js');

router.get('/', favoriteController.getAllFavorite);

module.exports = router;
