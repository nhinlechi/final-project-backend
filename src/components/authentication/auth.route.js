const router = require('express').Router();
const authController = require('./auth.controller.js');

router.post('/register', authController.registerUsername);
router.post('/checkregister', authController.checkRegisterUsername);
router.post('/loginfacebook', authController.loginfacebook);
router.post('/logingoogle', authController.logingoogle);
router.post('/login', authController.loginUsername);

module.exports = router;
