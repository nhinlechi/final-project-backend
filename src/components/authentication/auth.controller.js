const { REGISTER, LOGIN } = require('../../middlewares/type-response');
const { successResponse } = require('../../middlewares/success-response');
const {
    createUserValidation,
} = require('../../validations/user.validation.js');
const { catchAsync } = require('../../utils/catchAsync');
const { checkUsername } = require('../user/user.service');
const {
    loginValidation,
    checkRegisterValidation,
} = require('../../validations/auth.validation');
const {
    registerUser,
    login,
    loginGoogle,
    loginFacebook,
} = require('./auth.service');

// REGISTER ACCOUNT thought username password
exports.registerUsername = catchAsync(async (req, res) => {
    await createUserValidation(req.body);
    const data = await registerUser(req.body);
    successResponse(res, REGISTER, data, 201);
});

// check REGISTER ACCOUNT thought username password
exports.checkRegisterUsername = catchAsync(async (req, res) => {
    await checkRegisterValidation(req.body);
    await checkUsername(req.body.username);
    successResponse(res, 'OK');
});

// LOGIN username
exports.loginUsername = catchAsync(async (req, res) => {
    await loginValidation(req.body);
    const data = await login(req.body);
    successResponse(res, LOGIN, data);
});

// LOGIN google
exports.logingoogle = catchAsync(async (req, res) => {
    const data = await loginGoogle(req.body);
    successResponse(res, LOGIN, data);
});

// LOGIN facebook
exports.loginfacebook = catchAsync(async (req, res) => {
    const data = await loginFacebook(req.body);
    successResponse(res, LOGIN, data);
});
