const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const axios = require('axios');
const { OAuth2Client } = require('google-auth-library');
const AppError = require('../../utils/appError.js');
const { createUser, getUserByUsername } = require('../user/user.service');

// Genarate token
const genarateToken = (_id) =>
    jwt.sign({ _id }, process.env.TOKEN_SECRET, {
        expiresIn: process.env.TOKEN_EXPIRE,
    });

// Compare password
const comparePassword = async (password, hashPassword) => {
    const valid = await bcrypt.compare(password, hashPassword);
    if (!valid) throw new AppError(401, 'Password or username invalid');
    return true;
};

// Register
exports.registerUser = async (data) => {
    const user = await createUser(data);
    const token = genarateToken(user._id);
    return {
        token,
        first_name: user.firstname,
        userid: user._id,
        isNewUser: true,
    };
};

// Login
exports.login = async (data) => {
    const user = await getUserByUsername(data.username);
    if (!user) throw new AppError(401, 'Password or username invalid');
    await comparePassword(data.password, user.password);
    const token = genarateToken(user._id);
    return {
        token,
        first_name: user.firstname,
        userid: user._id,
        isNewUser: false,
    };
};

// Login Google
// https://developers.google.com/identity/sign-in/web/backend-auth
exports.loginGoogle = async (data) => {
    const client = new OAuth2Client(process.env.CLIENT_ID_WEB_FRONTEND);
    const ticket = await client.verifyIdToken({
        idToken: data.googleToken,

        // Specify the CLIENT_ID of the app that accesses the backend
        // Or, if multiple clients access the backend:
        // [CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
        audience: [
            process.env.CLIENT_ID_ANDROID,
            process.env.CLIENT_ID_WEB_FRONTEND,
            process.env.CLIENT_ID_WEB,
        ],
    });

    const payload = ticket.getPayload();
    const userid = payload.sub;
    const user = await getUserByUsername(userid);
    if (!user) {
        return await this.registerUser({
            lastname: payload.family_name,
            firstname: payload.given_name,
            username: userid,
            password: '000000',
        });
    }

    return await this.login({
        username: userid,
        password: '000000',
    });
};

// Login Facebook
exports.loginFacebook = async (data1) => {
    const { data } = await axios({
        url: 'https://graph.facebook.com/me',
        method: 'get',
        params: {
            fields: ['id', 'email', 'picture', 'first_name', 'last_name'].join(
                ','
            ), // save picture later
            access_token: data1.accesstoken,
        },
    });

    const user = await getUserByUsername(data.id);
    if (!user) {
        return await this.registerUser({
            firstname: data.first_name,
            lastname: data.last_name,
            username: data.id,
            password: '000000',
        });
    }

    return await this.login({
        username: data.id,
        password: '000000',
    });
};
