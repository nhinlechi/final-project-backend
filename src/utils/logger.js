const winston = require('winston');

const { combine, printf, colorize } = winston.format;

const logger = winston.createLogger({
    level: 'info',
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({
            filename: 'logs/info.log',
        }),
    ],
    format: combine(
        colorize(),
        winston.format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss',
        }),
        printf((log) => {
            const { message, timestamp, trace } = log;
            return `${timestamp} ${message}.${trace ? ` Trace: ${trace}` : ''}`;
        })
    ),
});

module.exports = logger;
