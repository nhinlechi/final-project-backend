const router = require('express').Router();

const authRoute = require('../components/authentication/auth.route');
const userRoute = require('../components/user/user.route');
const favoriteRoute = require('../components/favorite/favorite.route');
const occupationRoute = require('../components/occupation/occupation.route');
const firebaseRoute = require('../components/firebase/firebase.route');
const placeRoute = require('../components/place/place.route');
const groupRoute = require('../components/group/group.route');
const eventRoute = require('../components/event/event.router');
const storageRoute = require('../components/storage/storage.route');
const tagRoute = require('../components/tag/tag.router');
const matchRoute = require('../components/match/match.route');
const notificationRoute = require('../components/notification/notification.router');
const meetingHistoryRoute = require('../components/meeting-history/meeting-history.router');

const { verifyToken } = require('../middlewares/authentication');

router.use('/auth', authRoute);
router.use('/favorite', favoriteRoute);
router.use('/occupation', occupationRoute);
router.use('/tag', tagRoute);
router.use('/user', userRoute);
router.use(verifyToken);
router.use('/storage', storageRoute);
router.use('/notification', notificationRoute);
router.use('/firebase', firebaseRoute);
router.use('/place', placeRoute);
router.use('/group', groupRoute);
router.use('/group/:groupid/event', eventRoute);
router.use('/match/meetinghistory', meetingHistoryRoute);
router.use('/match', matchRoute);

module.exports = router;
