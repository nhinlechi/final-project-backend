const express = require('express');
const cors = require('cors');
const swaggerUi = require('swagger-ui-express');
const rateLimit = require('express-rate-limit');

const routes = require('./routes/index.js');
const AppError = require('./utils/appError.js');
const handlerError = require('./middlewares/error-response');
const { swaggerSpec } = require('./docs/swaggerSpec');

const app = express();

// Allow Cross-Origin requests
app.use(cors());

// Limit request from the same API
const limiter = rateLimit({
    windowMs: 1000, // 1 second
    max: 10, // limit each IP to 10 requests per windowMs
});

//app.use(limiter);

// Body parser, reading data from body into req.body
app.use(express.json());
app.use(express.urlencoded());

// Api swagger
app.use('/api/v1/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

// Routes
app.use('/api/v1', routes);

// handle undefined Routes
app.use('*', (req, res, next) => {
    const err = new AppError(404, 'undefined route');
    next(err);
});

app.use(handlerError);

module.exports = app;
