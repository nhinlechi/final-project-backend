const joi = require('@hapi/joi');
joi.objectId = require('joi-objectid')(joi);

exports.validation = {
    // COMMON
    id: joi.objectId(),
    // USER
    name: joi.string().min(1).max(225),
    description: joi.string().min(0).max(1024),
    username: joi.string().email(),
    password: joi.string().pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
    firstname: joi.string().min(1).max(30),
    lastname: joi.string().min(1).max(30),
    phone: joi.string().pattern(new RegExp('^[0-9]{10}$')),
    gender: joi.number().integer().min(1).max(3),
    relationship: joi.number().integer().min(1).max(3),
    dateofbirth: joi.number().integer(),
    lat: joi.number().max(90).min(-90),
    long: joi.number().max(180).min(-180),
    listliked: joi.array().items(joi.objectId()),
    historymatch: joi.array().items(joi.objectId()),
    friends: joi.array().items(joi.objectId()),
    avatar: joi.array().items(joi.string()).max(6),
    occupations: joi.array().items(joi.objectId()),
    favorites: joi.array().items(joi.objectId()),
    tags: joi.array().items(joi.objectId()),
    boolean: joi.boolean(),
    filetype: joi.string(),
    select: joi
        .array()
        .items(
            joi.valid(
                'listliked',
                'historymatch',
                'friends',
                'occupations',
                'favorites'
            )
        ),
    codeemail: joi.string().length(6),
    // MATCH
    minage: joi.number().max(100).min(18),
    maxage: joi.number().max(100).min(18),
    distance: joi.number().min(0).max(100),
    participants: joi.array().items(joi.objectId()),
    statusmatch: joi.valid(-2, -1, 0, 1),
    message: joi.string(),
    typemessage: joi.string(),
    lastmessage: joi.number().integer().min(0),
    statusmeetinghistory: joi.valid('-2', '-1', '0', '1'),
    timestampnumber: joi.number().integer(),
    // PLACE
    page: joi.number().integer().min(1),
    rowsperpage: joi.number().integer().min(1).max(100),
    type: joi.string().valid('bar', 'restaurant', 'cafe', 'shopping_mall'),
    sort: joi.number().integer().valid(1, -1),
    sortby: joi
        .when('sort', {
            then: joi.string().valid('name', 'rating').required(),
        })
        .forbidden(),
    comment: joi.string().max(100).min(1),
    rating: joi.number().integer().max(5).min(1),
    textsearch: joi.string().max(100),

    // NOTIFICATION
    statusconfirm: joi.boolean(),
    code: joi.number().integer().min(1).max(20),
    // GROUP
    groupavatar: joi.string(),
    eventstatus: joi.valid('0', '1'),
    // TAG
    tagtype: joi.valid('1', '2', '3'),
};
