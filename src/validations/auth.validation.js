const joi = require('@hapi/joi');
const { validation } = require('./validation');
const AppError = require('../utils/appError.js');

// LOGIN
exports.loginValidation = async (data) => {
    const schema = joi.object({
        username: validation.username.required(),
        password: validation.password.required(),
    });

    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};

// CHECK username, password register
exports.checkRegisterValidation = async (data) => {
    const schema = joi
        .object({
            username: validation.username.required(),
            password: validation.password.required(),
            confirmpassword: joi.ref('password'),
        })
        .with('password', 'confirmpassword');

    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};
