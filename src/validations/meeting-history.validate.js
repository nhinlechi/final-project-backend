const joi = require('@hapi/joi');
const { validation } = require('./validation');
const AppError = require('../utils/appError.js');

// CREATE
exports.getMeetingHisotyValidation = async (data) => {
    const schema = joi.object({
        matchid: validation.id,
        status: validation.statusmeetinghistory,
    });

    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};
