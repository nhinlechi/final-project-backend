const joi = require('@hapi/joi');
const { validation } = require('./validation');
const AppError = require('../utils/appError.js');

// RETRIVE
exports.selectFieldUserValidation = async (data) => {
    const schema = joi.object({ select: validation.select });
    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};

// CREATE
exports.createUserValidation = async (data) => {
    const schema = joi
        .object({
            username: validation.username.required(),
            password: validation.password.required(),
            confirmpassword: joi.ref('password'),
            firstname: validation.firstname.required(),
            lastname: validation.lastname.required(),
        })
        .with('password', 'confirmpassword');

    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};

exports.changePasswordValidation = async (data) => {
    const schema = joi
        .object({
            password: validation.password.required(),
            newpassword: validation.password.required(),
            confirmnewpassword: joi.ref('newpassword'),
        })
        .with('newpassword', 'confirmnewpassword');

    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};

exports.forgotPasswordValidation = async (data) => {
    const schema = joi.object({
        email: validation.username.required(),
    });

    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};

exports.resetNewPasswordValidation = async (data) => {
    const schema = joi
        .object({
            email: validation.username.required(),
            code: validation.codeemail.required(),
            newpassword: validation.password.required(),
            confirmnewpassword: joi.ref('newpassword'),
        })
        .with('newpassword', 'confirmnewpassword');

    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};

// UPDATE
exports.updateUserValidation = async (data) => {
    const schema = joi.object({
        firstname: validation.firstname,
        lastname: validation.lastname,
        phone: validation.phone,
        dateofbirth: validation.dateofbirth,
        gender: validation.gender,
        relationship: validation.relationship,
        lat: validation.lat,
        long: validation.long,
        avatar: validation.avatar,
        favorites: validation.favorites,
        occupations: validation.occupations,
    });

    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};

// UPDATE
exports.updateUserExtensionValidation = async (data) => {
    const schema = joi.object({
        listliked: validation.listliked,
        historymatch: validation.historymatch,
        friends: validation.friends,
    });

    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};

// Save firebase registration token
exports.saveFirebaseTokenValidation = async (data) => {
    const schema = joi.object({
        firebasetoken: validation.string().require(),
    });

    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};
