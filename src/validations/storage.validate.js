const joi = require('@hapi/joi');
const { validation } = require('./validation');
const AppError = require('../utils/appError.js');

exports.resignedURLStorageValidation = async (data) => {
    const schema = joi.object({
        type: validation.filetype.required(),
    });

    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};
