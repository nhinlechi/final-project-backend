const joi = require('@hapi/joi');
const { validation } = require('./validation');
const AppError = require('../utils/appError.js');

exports.idValidation = async (data) => {
    const schema = joi.object({
        id: validation.id.required(),
    });

    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};
