const joi = require('@hapi/joi');
const { validation } = require('./validation');
const AppError = require('../utils/appError.js');

// CREATE
exports.getAllNotificationValidation = async (data) => {
    const schema = joi.object({
        page: validation.page.required(),
        rowsperpage: validation.rowsperpage.required(),
        serviceid: validation.id,
        code: validation.code,
    });

    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
    data.page = parseInt(data.page, 10);
    data.rowsperpage = parseInt(data.rowsperpage, 10);
};
