const joi = require('@hapi/joi');
const { validation } = require('./validation');
const AppError = require('../utils/appError.js');

// GET PLACE
exports.getPlacesValidation = async (data) => {
    const schema = joi.object({
        page: validation.page.required(),
        rowsperpage: validation.rowsperpage.required(),
        type: validation.type,
        sort: validation.sort,
        sortby: validation.sortby,
        textsearch: validation.textsearch,
    });

    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};

// ADD COMMENT
exports.commentValidation = async (data) => {
    const schema = joi.object({
        id: validation.id.required(),
        comment: validation.comment,
        rating: validation.rating.required(),
    });

    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};

exports.getplacesInViewPortValidation = async (data) => {
    const schema = joi.object({
        lat_left_top: validation.lat.required(),
        long_left_top: validation.long.required(),
        lat_right_bottom: validation.lat.required(),
        long_right_bottom: validation.long.required(),
    });

    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};
