const joi = require('@hapi/joi');
const { validation } = require('./validation');
const AppError = require('../utils/appError.js');

// CREATE
exports.requestMatchValidation = async (data) => {
    const schema = joi.object({
        lat: validation.lat.required(),
        long: validation.long.required(),
        distance: validation.distance,
        gender: validation.gender,
        minage: validation.minage,
        maxage: validation.maxage,
        relationship: validation.relationship,
    });

    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};

// CREATE
exports.confirmMatchNowValidation = async (data) => {
    const schema = joi.object({
        id: validation.id.required(),
        status: validation.statusconfirm.required(),
    });

    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};

exports.matchedConfirmValidation = async (data) => {
    const schema = joi.object({
        matchid: validation.id.required(),
        status: validation.boolean.required(),
    });
    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};

exports.getChatValidation = async (data) => {
    const schema = joi.object({
        matchId: validation.id.required(),
        lastMessage: validation.lastmessage,
    });
    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};

exports.sendPlaceRequestValidation = async (data) => {
    const schema = joi.object({
        matchid: validation.id.required(),
        placeid: validation.id.required(),
        date: validation.timestampnumber.required(),
    });
    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
    if (data.date - Date.now() < 1000 * 60 * 30)
        throw new AppError(
            400,
            'date must in the future (data timestamp - date now >  30 minutes )'
        );
};

exports.confirmPlaceValidation = async (data) => {
    const schema = joi.object({
        status: validation.statusconfirm.required(),
        notificationid: validation.id.required(),
    });
    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};
