const joi = require('@hapi/joi');
const { validation } = require('./validation');
const AppError = require('../utils/appError.js');

// CREATE
exports.createEventValidation = async (data) => {
    const schema = joi.object({
        name: validation.name.required(),
        description: validation.description,
        placeid: validation.id.required(),
        date: validation.timestampnumber.required(),
        groupid: validation.id.required(),
    });
    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
    if (data.date - Date.now() < 1000 * 60 * 30)
        throw new AppError(
            400,
            'date must in the future (data timestamp - date now >  30 minutes )'
        );
};

exports.getEventValidation = async (data) => {
    const schema = joi.object({
        page: validation.page.required(),
        rowsperpage: validation.rowsperpage.required(),
        status: validation.eventstatus,
        groupid: validation.id.required(),
    });
    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
    data.page = parseInt(data.page, 10);
    data.rowsperpage = parseInt(data.rowsperpage, 10);
};

exports.getEventDetailValidation = async (data) => {
    const schema = joi.object({
        groupid: validation.id.required(),
        id: validation.id.required(),
    });
    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};

exports.updateEventValidation = async (data) => {
    const schema = joi.object({
        eventid: validation.id.required(),
        groupid: validation.id.required(),
        name: validation.name,
        description: validation.description,
        placeid: validation.id,
        date: validation.timestampnumber,
    });
    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
    if (data.date - Date.now() < 1000 * 60 * 30)
        throw new AppError(
            400,
            'date must in the future (data timestamp - date now >  30 minutes )'
        );
};

exports.deleteEventValidation = async (data) => {
    const schema = joi.object({
        eventid: validation.id.required(),
        groupid: validation.id.required(),
    });
    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};

exports.joinEventValidation = async (data) => {
    const schema = joi.object({
        eventid: validation.id.required(),
        status: validation.boolean.required(),
        groupid: validation.id.required(),
    });
    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};

exports.getChatValidation = async (data) => {
    const schema = joi.object({
        eventid: validation.id.required(),
        groupid: validation.id.required(),
        lastMessage: validation.lastmessage,
    });
    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};
