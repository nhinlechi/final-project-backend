const joi = require('@hapi/joi');
const { validation } = require('./validation');
const AppError = require('../utils/appError.js');

// CREATE
exports.getAllGroupValidation = async (data) => {
    const schema = joi.object({
        page: validation.page.required(),
        rowsperpage: validation.rowsperpage.required(),
        tag: validation.id,
        sort: validation.sort,
        isprivate: validation.boolean,
        textsearch: validation.textsearch,
    });

    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
    data.page = parseInt(data.page, 10);
    data.rowsperpage = parseInt(data.rowsperpage, 10);
};

exports.createGroupValidation = async (data) => {
    const schema = joi.object({
        name: validation.name.required(),
        description: validation.description,
        isprivate: validation.boolean,
        avatar: validation.groupavatar,
        tag: validation.tags,
    });
    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};

exports.updateGroupValidation = async (data) => {
    const schema = joi.object({
        groupid: validation.id.required(),
        name: validation.name,
        description: validation.description,
        isprivate: validation.boolean,
        avatar: validation.groupavatar,
        tag: validation.tags,
    });
    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};

exports.requestJoinGroupValidation = async (data) => {
    const schema = joi.object({
        groupid: validation.id.required(),
        description: validation.description,
    });
    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};

exports.confirmJoinGroupValidation = async (data) => {
    const schema = joi.object({
        status: validation.boolean.required(),
        notificationid: validation.id.required(),
    });
    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};

exports.removeParticipantValidation = async (data) => {
    const schema = joi.object({
        groupid: validation.id.required(),
        participantid: validation.id.required(),
    });
    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};

exports.leaveValidation = async (data) => {
    const schema = joi.object({
        groupid: validation.id.required(),
    });
    const valid = await schema.validate(data);
    if (valid.error) {
        throw new AppError(400, valid.error.details[0].message);
    }
};
