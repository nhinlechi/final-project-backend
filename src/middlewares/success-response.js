exports.successResponse = (res, message, data = null, status = 200) => {
    res.status(status).json({
        result: true,
        message,
        data,
        err: null,
    });
};
