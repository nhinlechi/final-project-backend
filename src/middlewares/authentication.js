const jwt = require('jsonwebtoken');
const AppError = require('../utils/appError');
const { getUserById } = require('../components/user/user.service');

const throwTokenError = (message) => new AppError(403, message);

exports.verifyToken = async (req, res, next) => {
    try {
        const token = req.header('auth-token');
        const verified = jwt.verify(token, process.env.TOKEN_SECRET);

        const user = await getUserById(verified._id);
        if (!user) return next(throwTokenError('Token invalid'));

        req.user = verified;
        next();
    } catch (error) {
        return next(throwTokenError('Token invalid or expired'));
    }
};

exports.authenSocket = async (socket, next) => {
    try {
        const { token } = socket.handshake.query;
        const verified = jwt.verify(token, process.env.TOKEN_SECRET);

        const user = await getUserById(verified._id);
        const userquery = {
            _id: user._id.toString(),
            firstname: user.firstname,
            lastname: user.lastname,
            avatar: user.avatar,
        };
        socket.handshake.query.user = userquery;
        next();
    } catch (error) {
        return next(throwTokenError('Token invalid or expired'));
    }
};
