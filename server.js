const dotenv = require('dotenv');
const mongoose = require('mongoose');
const http = require('http');
const app = require('./src/app');
const logger = require('./src/utils/logger');
const socket = require('./src/components/socket-io');

// config with .env
dotenv.config();

const server = new http.Server(app);

socket(server);

// connect to Database
mongoose.connect(process.env.CONNECT_DB, { useNewUrlParser: true }, () => {
    logger.info('Connect to DB');
});

server.listen(process.env.PORT, () => {
    logger.info(`Socker listening on port ${process.env.PORT}!`);
});
